//题目链接：https://leetcode.cn/problems/valid-parentheses/

typedef char STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;
	int capacity;
}ST;
// 初始化栈
void STInit(ST* pst);
// 销毁栈
void STDestroy(ST* pst);
// 添加数据
void STPush(ST* pst, STDataType x);
// 删除数据
void STPop(ST* pst);
// 弹出数据
STDataType STTop(ST* pst);
// 判断是否为空
bool STEmpty(ST* pst);
// 判断大小
int STSize(ST* pst);
// 初始化栈
void STInit(ST* pst)
{
	assert(pst);
	pst->a = NULL;
	pst->top = 0;
	pst->capacity = 0;
}
// 销毁栈
void STDestroy(ST* pst)
{
	assert(pst);
	free(pst->a);
	pst->a = NULL;
	pst->top = 0;
	pst->capacity = 0;
}
// 添加数据
void STPush(ST* pst, STDataType x)
{
	if (pst->capacity == pst->top)
	{
		int newcapacity = (pst->capacity == 0 ? 4 : pst->capacity * 2);
		STDataType* tmp = (STDataType*)realloc(pst->a, newcapacity * sizeof(STDataType));
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}
		pst->a = tmp;
		pst->capacity = newcapacity;
	}
	pst->a[pst->top] = x;
	pst->top++;
}
// 删除数据
void STPop(ST* pst)
{
	assert(pst);
	assert(!(STEmpty(pst)));
	pst->top--;
}
// 弹出数据
STDataType STTop(ST* pst)
{
	assert(pst);
	assert(!(STEmpty(pst)));
	return pst->a[pst->top - 1];
}
// 判断是否为空
bool STEmpty(ST* pst)
{
	assert(pst);
	return pst->top == 0;
}
// 判断大小
int STSize(ST* pst)
{
	assert(pst);
	return pst->top;
}
bool isValid(char* s) {
	ST st;
	STInit(&st);
	while (*s)
	{
		// 左括号入栈
		if (*s == '(' || *s == '{' || *s == '[')
		{
			STPush(&st, *s);
		}
		// 右括号出栈匹配
		else
		{
			if (STEmpty(&st))
			{
				STDestroy(&st);
				return false;
			}
			else
			{
				char tmp = STTop(&st);
				STPop(&st);
				if (*s == ']' && tmp != '[' || *s == ')' && tmp != '(' || *s == '}' && tmp != '{')
				{
					STDestroy(&st);
					return false;
				}
			}
		}
		++s;
	}
	bool ret = STEmpty(&st);
	STDestroy(&st);
	return ret;
}