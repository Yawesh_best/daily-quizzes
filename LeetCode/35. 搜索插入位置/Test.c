//35. ��������λ�ã�https://leetcode.cn/problems/search-insert-position/description/
int searchInsert(int* nums, int numsSize, int target) {
    int left = 0, right = numsSize - 1, ans = numsSize;
    while (left <= right) {
        int mid = (right + left) / 2;
        if (target <= nums[mid]) {
            ans = mid;
            right = mid - 1;
        }
        else {
            left = mid + 1;
        }
    }
    return ans;
}