//101. �Գƶ�������https://leetcode.cn/problems/symmetric-tree/description/
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
bool _isSymmetric(struct TreeNode* leftroot, struct TreeNode* rightroot)
{
    if (leftroot == NULL && rightroot == NULL)
    {
        return true;
    }
    if (leftroot == NULL || rightroot == NULL)
    {
        return false;
    }
    if (leftroot->val != rightroot->val)
    {
        return false;
    }
    return _isSymmetric(leftroot->left, rightroot->right) && _isSymmetric(leftroot->right, rightroot->left);
}
bool isSymmetric(struct TreeNode* root) {
    return _isSymmetric(root->left, root->right);
}