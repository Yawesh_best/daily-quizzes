//面试题 17.16. 按摩师：https://leetcode.cn/problems/the-masseuse-lcci/description/
//动态规划
int massage(int* nums, int numsSize)
{
    if (numsSize == 0) return 0;
    if (numsSize == 1) return nums[0];
    if (numsSize == 2) return nums[0] > nums[1] ? nums[0] : nums[1];
    int* dp = (int*)malloc(sizeof(int) * numsSize);
    dp[0] = nums[0];
    dp[1] = nums[1];
    dp[2] = dp[0] + nums[2];
    for (int i = 3; i < numsSize; i++)
    {
        int max = dp[i - 2] > dp[i - 3] ? dp[i - 2] : dp[i - 3];
        dp[i] = max + nums[i];
    }
    return dp[numsSize - 1] > dp[numsSize - 2] ? dp[numsSize - 1] : dp[numsSize - 2];
}