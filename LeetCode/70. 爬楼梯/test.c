//70. ��¥�ݣ�https://leetcode.cn/problems/climbing-stairs/description/

int climbStairs(int n) {
    if (n < 0) return 0;
    if (n <= 2) return n;
    int fn1 = 1, fn2 = 2;
    int fn3;
    for (int i = 2; i < n; i++) {
        fn3 = fn2 + fn1;
        fn1 = fn2;
        fn2 = fn3;
    }
    return fn3;
}
