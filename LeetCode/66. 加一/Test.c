//��Ŀ���ӣ�https://leetcode.cn/problems/plus-one/description/
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* plusOne(int* digits, int digitsSize, int* returnSize)
{
    int jw = 1;
    int i;
    for (i = digitsSize - 1; i >= 0; i--) {
        digits[i] = digits[i] + jw;
        jw = digits[i] / 10;
        digits[i] = digits[i] % 10;
    }
    *returnSize = digitsSize + jw;
    int* sum = (int*)malloc(sizeof(int) * *returnSize);
    memset(sum, 0, sizeof(int) * *returnSize);
    for (i = digitsSize - 1; i >= 0; i--) {
        sum[i + jw] = digits[i];
    }
    sum[0] += jw;
    return sum;
}
/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
 /*int* plusOne(int* digits, int digitsSize, int* returnSize){
     int i = 0;
     int* d = digits;
     for(i = 0; i < digitsSize; i++)
     {
         if(i == digitsSize-1)
         {
             int j = i;
             while(digits[j] == 9)
             {
                 --j;
                 if(j < 0)
                 {
                     digits = (int*)malloc(sizeof(int) * (digitsSize++));

                     for(i = 0;i < digitsSize++ ;i++)
                     {
                         if(i==0)
                             digits[i] = 1;
                         else
                         {
                             digits[i] = 0;
                         }
                     }
                     goto end;
                 }
             }
             digits[i] = digits[i]+1;
         }
     }
     end:
     *returnSize = digitsSize;
     int* sum = (int*)malloc(sizeof(int)* *returnSize);
     memset(sum, 0, sizeof(int) * *returnSize);
     for(int i = 0; i<digitsSize;i++)
     {
         sum[i] = digits[i];
     }
     return sum;
 }*/
