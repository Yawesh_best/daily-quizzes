//LCP 06. ��Ӳ�ң�https://leetcode.cn/problems/na-ying-bi/description/
int minCount(int* coins, int coinsSize) {
    int i, count = 0;
    for (i = 0; i < coinsSize; i++)
    {

        while (coins[i] != 0 && coins[i] != 1)
        {
            count++;
            coins[i] -= 2;
        }
        if (coins[i] == 1)
        {
            count++;
        }
    }
    return count;
}