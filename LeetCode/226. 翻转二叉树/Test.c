//226. ��ת��������https://leetcode.cn/problems/invert-binary-tree/description/
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
struct TreeNode* invertTree(struct TreeNode* root) {
    if (root == NULL)
    {
        return NULL;
    }
    if (root->left == NULL && root->right == NULL)
    {
        return root;
    }

    //swap(root->right, root->left);
    struct TreeNode* tmp = root->right;
    root->right = root->left;
    root->left = tmp;
    struct TreeNode* right = invertTree(root->right);
    struct TreeNode* left = invertTree(root->left);
    return root;
}