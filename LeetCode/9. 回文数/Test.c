//9. ��������https://leetcode.cn/problems/palindrome-number/description/

bool isPalindrome(int x) {
    if (x < 0)
        return false;
    int i, j, k = 0;
    char arr[100];
    sprintf(arr, "%d", x);
    int n = strlen(arr) - 1;
    while (k < n)
    {
        if (arr[k++] != arr[n--])
        {
            return false;
        }
    }
    return true;

}