//13. 罗马数字转整数：https://leetcode.cn/problems/roman-to-integer/description/
/*int romanToInt(char * s){
    int i = 1；
    int v = 5;
    int x = 10;
    int l = 50;
    int c = 100;
    int d = 500;
    int m = 1000;
    int iv = 4;
    int ix = 9;

    int j, sum = 0;
    for(j = 0; j < strlen(s); j++)
    {
        if(s[j] == 'I' && s[j+1] != 'V')
        {
            sum += i;
        }
        if(s[j] == 'I' && s[j+1] == 'V')
        {
            sum += iv;
            j++;
        }
        if(s[j] == 'I' && s[j+1] == 'X')
        {
            sum += ix;
            j++;
        }
        if(s[j] == 'X' && s[j+1] == 'l')
        {
            sum += iv;
            j++;
        }
    }
}*/
int romanToInt(char* s) {
    int symbolValues[26];
    symbolValues['I' - 'A'] = 1;
    symbolValues['V' - 'A'] = 5;
    symbolValues['X' - 'A'] = 10;
    symbolValues['L' - 'A'] = 50;
    symbolValues['C' - 'A'] = 100;
    symbolValues['D' - 'A'] = 500;
    symbolValues['M' - 'A'] = 1000;
    int ans = 0;
    int n = strlen(s);
    for (int i = 0; i < n; ++i) {
        int value = symbolValues[s[i] - 'A'];
        if (i < n - 1 && value < symbolValues[s[i + 1] - 'A']) {
            ans -= value;
        }
        else {
            ans += value;
        }
    }
    return ans;
}