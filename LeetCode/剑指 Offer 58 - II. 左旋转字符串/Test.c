//��ָ Offer 58 - II. ����ת�ַ�����https://leetcode.cn/problems/zuo-xuan-zhuan-zi-fu-chuan-lcof/description/
char* reverseLeftWords(char* s, int n) {
    int k = n;
    int num = strlen(s);
    while (k--)
    {
        char tmp = s[0];
        for (int i = 0; i < num - 1; i++)
        {
            s[i] = s[i + 1];
        }
        s[num - 1] = tmp;
    }
    return s;
}