//104. 二叉树的最大深度  题目链接：https://leetcode.cn/problems/maximum-depth-of-binary-tree/description/
int maxDepth(struct TreeNode* root) {
    if (root == NULL)
        return 0;
    int maxDepthright = maxDepth(root->right);
    int maxDepthleft = maxDepth(root->left);
    return maxDepthleft > maxDepthright ? maxDepthleft + 1 : maxDepthright + 1;
}