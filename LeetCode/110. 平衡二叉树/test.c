//110. ƽ������� ��https://leetcode.cn/problems/balanced-binary-tree/description/
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
 */
int _isBalanced(struct TreeNode* root)
{
    if (root == NULL)
    {
        return 0;
    }
    int lefthigh = _isBalanced(root->left);
    int righthigh = _isBalanced(root->right);
    if (lefthigh == -1 || righthigh == -1 || abs(lefthigh - righthigh) > 1)
    {
        return -1;
    }
    return (lefthigh > righthigh ? lefthigh : righthigh) + 1;

}
bool isBalanced(struct TreeNode* root) {
    return _isBalanced(root) >= 0;
}