//https://leetcode.cn/problems/maximum-points-inside-the-square/submissions/552193233/?envType=daily-question&envId=2024-08-03


class Solution {
public:
    int maxPointsInsideSquare(vector<vector<int>>& points, string s) {
        int n = points.size(), ans = 0;
        vector<int>edge;
        vector<int>index(n);
        vector<bool>mp(26, false);
        for(auto& point : points){
            edge.push_back(max(abs(point[0]), abs(point[1])));
        }
        for(int i = 0;i < n;i++){
            index[i] = i;
        }
        sort(index.begin(), index.end(), [&](int a, int b){
            return edge[a] < edge[b];
        });
        sort(edge.begin(), edge.end());
        edge.push_back(INT_MAX);
        index.push_back(0);
        int r = edge[0], pre = 0;
        mp[s[index[0]] - 'a'] = true;
        for(int i = 1;i < n + 1;i++){
            int rnew = edge[i];
            if(rnew == r){
                if(mp[s[index[i]] - 'a']) break;
            }
            else{
                ans += i - pre;
                pre = i;
                r = rnew;
                if(mp[s[index[i]] - 'a']) break;
            }
            mp[s[index[i]] - 'a'] = true;
        }
        return ans;
    }
};