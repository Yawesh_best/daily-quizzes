//https://leetcode.cn/problems/distribute-candies/description/?envType=daily-question&envId=2024-06-02


class Solution {
public:
    int distributeCandies(vector<int>& candyType) {
        int n = candyType.size(), maxnum = candyType.size()/2, ans = 0;
        unordered_set<int> st;
        for(int i = 0; i < n; i++)
        {
            if(st.count(candyType[i]) > 0)
            {
                continue;
            } 
            else
            {
                ans++;
                st.insert(candyType[i]);
            }
        }
        return min(ans, maxnum);
    }
};