//https://leetcode.cn/problems/serialize-and-deserialize-bst/?envType=daily-question&envId=2023-09-04


TreeNode* node;
class Codec {
public:

    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        node = root;
        return "";
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
        return node;
    }
};