//https://leetcode.cn/problems/remove-duplicates-from-sorted-list/description/?envType=daily-question&envId=2024-01-14


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        if(head== nullptr) return nullptr;
        ListNode* node = head->next;
        ListNode* prv = head;
        while(node)
        {
            if(prv->val != node->val) 
            {
                prv->next = node;
                prv = node;
            }
            node = node->next; 
        }
        if(prv->next && prv->val == prv->next->val) prv->next = nullptr;
        return head;
    }
};