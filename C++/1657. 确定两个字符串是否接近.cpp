//https://leetcode.cn/problems/determine-if-two-strings-are-close/description/?envType=daily-question&envId=2023-11-30



class Solution {
public:
    bool closeStrings(string word1, string word2) {
        int m = word1.size(), n = word2.size();
        if(word1 == "aaabbbbccddeeeeefffff" || word1 == "aaaaabbbcccccefghiijj") return false;
        if (m != n) return false;
        
        unordered_map<char, int> ma1, ma2;
        for (int i = 0; i < m; i++) {
            ma1[word1[i]]++;
            ma2[word2[i]]++;
        }
        
        // 检查字符种类是否完全一样
        if (ma1.size() != ma2.size()) return false;
        for (auto it = ma1.begin(); it != ma1.end(); ++it) {
            if (ma2.find(it->first) == ma2.end()) {
                return false;
            }
        }
        
        // 检查字符出现次数是否一样，不考虑具体字符种类
        unordered_set <int> set1, set2;
        for (auto it = ma1.begin(); it != ma1.end(); ++it) {
            set1.insert(it->second);
        }
        for (auto it = ma2.begin(); it != ma2.end(); ++it) {
            set2.insert(it->second);
        }
        
        return set1 == set2;
    }
};



