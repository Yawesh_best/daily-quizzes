//https://leetcode.cn/problems/avoid-flood-in-the-city/?envType=daily-question&envId=2023-10-13

class Solution {
public:
    vector<int> avoidFlood(vector<int>& rains) {
        int n = rains.size();
        vector<int> ans(n, 1);
        map<int, int> water;
        set<int>zero;
        for (int i = 0; i < n; ++i) 
        {
           if (rains[i] == 0)
           {
               zero.insert(i);
               continue;
           }
           if (water.count(rains[i]) != 0) 
           {
                auto it = zero.lower_bound(water[rains[i]]);
                if (it == zero.end()) return {};
                ans[*it] = rains[i];
                zero.erase(it);
            }
            water[rains[i]] = i;
            ans[i] = -1;
       }
       return ans;
    }
};