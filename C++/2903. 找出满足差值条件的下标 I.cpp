//https://leetcode.cn/problems/find-indices-with-index-and-value-difference-i/?envType=daily-question&envId=2024-05-25


class Solution {
public:
    vector<int> findIndices(vector<int>& nums, int indexDifference, int valueDifference) {
        vector<int> ans = {-1, -1};
        int n = nums.size();
        for(int i = 0; i < n; i++)
        {
            for(int j = i + indexDifference; j < n; j++)
            {
                if(abs(nums[i] - nums[j]) >= valueDifference)
                {
                    ans = {i, j};
                    break;
                }
            }
        }
        
        return ans;
    }
};