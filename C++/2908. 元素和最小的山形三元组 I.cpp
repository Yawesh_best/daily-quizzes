//https://leetcode.cn/problems/minimum-sum-of-mountain-triplets-i/description/?envType=daily-question&envId=2024-03-29https://leetcode.cn/problems/minimum-sum-of-mountain-triplets-i/description/?envType=daily-question&envId=2024-03-29

class Solution {
public:
    int minimumSum(vector<int>& nums) {
        int count{numeric_limits<int>::max()};
        for(int i{};i<nums.size()-2;++i)
            for(int j{i+1};j<nums.size()-1;++j)
                for(int k{j+1};k<nums.size();++k)
                    if(nums[i] < nums[j] && nums[k] < nums[j]) count = min(count,nums[i]+nums[j]+nums[k]);
        return count == numeric_limits<int>::max() ? -1 :count;
    }
};