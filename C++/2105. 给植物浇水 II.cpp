//https://leetcode.cn/problems/watering-plants-ii/description/?envType=daily-question&envId=2024-05-09https://leetcode.cn/problems/watering-plants-ii/description/?envType=daily-question&envId=2024-05-09


class Solution {
public:
    int minimumRefill(vector<int>& plants, int capacityA, int capacityB) {
        int ans = 0, left = 0, right = plants.size() - 1, a = capacityA, b = capacityB;
        while(left < right)
        {
            if(a < plants[left]){
                a = capacityA - plants[left];
                ans++;
            }else{
                a -= plants[left];
            }
            if(b < plants[right]){
                b = capacityB - plants[right];
                ans++;
            }else{
                b -= plants[right];
            }
            left++;
            right--;
        }
        if(left == right)
        {
            int tmp = max(a, b);
            if(tmp < plants[left]) ans++;
        }
        return ans;
    }
};