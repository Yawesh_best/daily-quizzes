//https://leetcode.cn/problems/find-longest-special-substring-that-occurs-thrice-ii/description/?envType=daily-question&envId=2024-05-30https://leetcode.cn/problems/find-longest-special-substring-that-occurs-thrice-ii/description/?envType=daily-question&envId=2024-05-30

class Solution {
public:
    int maximumLength(string s) {
        unordered_map<char,vector<int>>map;
        int count = 0;
        int temp = 0;
        for(int i = 0;i < s.size();i++)
        {
            temp++;
            if(i==s.size()-1 || s[i+1] != s[i])
            {
                map[s[i]].push_back(temp);
                temp = 0;
            }
        } 
        for(auto it = map.begin();it != map.end();it++)
        {
            sort(it->second.begin(),it->second.end(),greater<int>());

            it->second.push_back(0);
            it->second.push_back(0);
            
            count = max(max(it->second[0]-2,max(min(it->second[1],it->second[0]-1),it->second[2])),count);
        }
        if(count == 0)return -1;
        return count;
    }
};