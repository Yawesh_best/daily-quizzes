//https://leetcode.cn/problems/find-the-peaks/description/?envType=daily-question&envId=2024-05-28



class Solution {
public:
    vector<int> findPeaks(vector<int>& mountain) {
        vector<int> ans;
        if(mountain.size() <= 2) return ans;
        for(int i = 1; i < mountain.size() - 1; i++)
        {
            if(mountain[i] > mountain[i - 1] && mountain[i] > mountain[i + 1])
                ans.push_back(i);
        }
        return ans;
    }
};