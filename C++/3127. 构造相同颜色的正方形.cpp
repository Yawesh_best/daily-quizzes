//https://leetcode.cn/problems/make-a-square-with-the-same-color/description/?envType=daily-question&envId=2024-08-31

class Solution {
public:
    bool canMakeSquare(vector<vector<char>>& grid) {
        for(int i = 1;i < 3;++i)
            for(int j = 1;j < 3;++j)
                if((grid[i][j] ^ grid[i-1][j] ^ grid[i][j-1] ^ grid[i-1][j-1]) == ('W' ^ 'B')
                || grid[i][j] == grid[i-1][j] && grid[i-1][j] == grid[i][j-1] && grid[i][j-1] == grid[i-1][j-1])
                    return true;
        return false;
    }
};

