//https://leetcode.cn/problems/find-the-power-of-k-size-subarrays-ii/description/?envType=daily-question&envId=2024-11-07


class Solution {
public:
    vector<int> resultsArray(vector<int>& nums, int k) {
        int n = nums.size();
        vector<int>ans(n - k + 1, -1);
        if(k == 1) return nums;
        for(int l = 0, r = 1; r < nums.size(); r++){
            while(nums[r] - nums[r - 1] != 1 && l + k - 1 < n && l < r){
                l++;
            }
            if(r - l + 1 == k){
                ans[l++] = nums[r];
            }
        }
        return ans;
    }
};