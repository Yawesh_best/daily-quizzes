//https://leetcode.cn/problems/rings-and-rods/?envType=daily-question&envId=2023-11-02



class Solution {
public:
    int countPoints(string rings) {
        unordered_map<char, set<char>> m;
        int ans = 0, n = rings.size();
        for(int i = 1; i <= n; i += 2)
        {
            m[rings[i]].insert(rings[i - 1]);
        }
        for(auto str: m)
        {
            if(str.second.size() == 3)
            {
                ans++;
            }
        }
        return ans;
    }
};