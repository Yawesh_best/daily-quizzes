//https://www.nowcoder.com/practice/43072d50a6eb44d2a6c816a283b02036?tpId=37&tqId=21294&ru=/exam/oj


#include <iostream>
#include <string>
#include <vector>

using namespace std;

bool iscommon(char input)
{
    if((input >= 'A' && input <= 'Z') || (input >= '0' && input <= '9'))
    {
        return true;
    }
    else 
    {return false;}
}

char changeAlpha(char input)
{
    if(input >= 'a' && input <= 'z')
    {
        int k = input - 'a';
        return 'A'+k;
    }
    else return input;
}

int main()
{
    string str2, str1;
    cin >> str2 >> str1;

    for(int i = 0; i < str1.size(); i++)
    {
        str1[i] = changeAlpha(str1[i]);
    }

    for(int j = 0; j < str2.size(); j++)
    {
        str2[j] = changeAlpha(str2[j]);
    }

    int n = str1.size();
    int m = str2.size();
    
    str1.insert(str1.begin(), '!');
    str2.insert(str2.begin(), '!');
    vector<vector<int>> dp(n+1, vector<int>(m+1));

    dp[0][0] = 1;
    if(str2[1] == '*') dp[0][1] = 1;
    for(int i = 1; i <= n; i++)
    {
        dp[i][0] = 0;
    }
    for(int j = 2; j <= m; j++)
    {
        dp[0][j] = 0;
    }

    for(int i = 1; i <= n; i++)
    for(int j = 1; j <= m; j++)
    {
        if(iscommon(str1[i]))
        {
            if(iscommon(str2[j]))
            {
                if(str1[i] == str2[j])
                {dp[i][j] = dp[i-1][j-1];}
                else {
                    dp[i][j] = 0; 
                }
            }
            else if(str2[j] == '?')
            {
                dp[i][j] = dp[i-1][j-1];
            }
            else if(str2[j] == '*')
            {
                dp[i][j] = dp[i-1][j-1] || dp[i-1][j] || dp[i][j-1];
            }
        }
        else {
            if(str1[i] == str2[j])
            {
                dp[i][j] = dp[i-1][j-1];
            }
            else {
                dp[i][j] = 0;
            }
        }
    }

    if(dp[n][m] == 1) cout << "true" << endl;
    else cout << "false" << endl;
}