//https://leetcode.cn/problems/take-gifts-from-the-richest-pile/?envType=daily-question&envId=2023-10-28


class Solution {
public:
    long long pickGifts(vector<int>& gifts, int k) {
        priority_queue<int> q(gifts.begin(), gifts.end());
        while(k--)
        {
            int tmp = q.top();
            q.pop();
            tmp = sqrt(tmp);
            q.push(tmp);
        }
        long long ans = 0;
        while(!q.empty())
        {
            int tmp = q.top();
            q.pop();
            ans += tmp;
        }
        return ans;
    }
};