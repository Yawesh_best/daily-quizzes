//https://leetcode.cn/problems/reverse-odd-levels-of-binary-tree/description/?envType=daily-question&envId=2023-12-15


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* reverseOddLevels(TreeNode* root) {
        stack<int> num;
        queue<TreeNode*> que;
        int level = 0;
        que.push(root);
        while(!que.empty())
        {
            int sz = que.size();
            for(int i = 0; i < sz; i++)
            {
                TreeNode* ptr = que.front();
                que.pop(); 
                if(level % 2 != 0)
                {
                    ptr->val = num.top();
                    num.pop();
                }
                if(ptr->left)
                {
                    if(level % 2 == 0)
                    {
                        num.push(ptr->left->val);
                        num.push(ptr->right->val);
                    }
                    que.push(ptr->left);
                    que.push(ptr->right);
                }
            }
            level++;
        }
        return root;
    }
};
