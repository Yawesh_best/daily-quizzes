//https://leetcode.cn/problems/maximum-sum-with-exactly-k-elements/?envType=daily-question&envId=2023-11-15



class Solution {
public:
    int maximizeSum(vector<int>& nums, int k) {
        int ans = nums[0];
        for(int i = 0; i < nums.size(); i++)
            if(nums[i] > ans) ans = nums[i];
        int tmp = ans;
        for(int i = 1; i < k; i++)
            ans += tmp + i;
        return ans;
    }
};