//https://leetcode.cn/problems/count-of-integers/?envType=daily-question&envId=2024-01-16https://leetcode.cn/problems/count-of-integers/?envType=daily-question&envId=2024-01-16


static constexpr long long mod = 1e9 + 7;
using LL = long long;
class Solution {
   public:
    int Min_sum, Max_sum;
    LL calc(string s) {
        LL m = s.size();
        vector memo(25, vector<LL>(450, -1));
        function<LL(LL, LL, bool, bool)> dfs = [&](LL i, LL cnt, bool is_limit, bool is_num) -> LL {
            if (i == m) return cnt >= Min_sum and cnt <= Max_sum;
            if (!is_limit and is_num and memo[i][cnt] != -1) return memo[i][cnt];
            LL res = 0;
            if (!is_num) {
                res += dfs(i + 1, cnt, false, false);
                res %= mod;
            }
            int up = is_limit ? s[i] - '0' : 9;
            int low = is_num ? 0 : 1;
            for (int d = low; d <= up; ++d) {
                res += dfs(i + 1, cnt + d, is_limit and d == up, true);
                res %= mod;
            }
            if (!is_limit and is_num) memo[i][cnt] = res;
            return res;
        };
        return dfs(0, 0, true, false);
    }
    bool check(string s) {
        int cnt = 0;
        for (char c : s) cnt += c - '0';
        return cnt >= Min_sum and cnt <= Max_sum;
    }
    int count(string num1, string num2, int min_sum, int max_sum) {
        Max_sum = max_sum;
        Min_sum = min_sum;
        LL ans = calc(num2) - calc(num1) + check(num1);
        return (ans % mod + mod) % mod;
    }
};

