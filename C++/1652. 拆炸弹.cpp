//https://leetcode.cn/problems/defuse-the-bomb/?envType=daily-question&envId=2024-05-05


#include <ranges>
class Solution {
public:
    vector<int> decrypt(vector<int>& code, int k) {
        int n = code.size();
        vector<int> ans(n);
        if (k == 0) {
            return ans;
        }
        vector sum(2 * n + 1, 0);
        for (int i : views::iota(1, 2 * n + 1)) {
            sum[i] = sum[i - 1] + code[(i - 1) % n];
        }
        for (int i : views::iota(1, n + 1)) {
            ans[i - 1] = k < 0 ? sum[i + n - 1] - sum[i + n + k - 1] : sum[i + k] - sum[i];
        }
        return ans;
    }
};