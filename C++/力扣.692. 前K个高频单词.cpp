//https://leetcode.cn/problems/top-k-frequent-words/


class Solution {
public:
    struct Compare
    {
        bool operator()(const pair<string, int>& kv1, const pair<string, int>& kv2)
        {
            return kv1.second > kv2.second;
        }
    };
    vector<string> topKFrequent(vector<string>& words, int k) {
        map<string, int> countMap;
        for(auto& str : words)
        {
            countMap[str]++;
        }
        vector<pair<string, int>> v(countMap.begin(), countMap.end());
        stable_sort(v.begin(), v.end(), Compare());
        vector<string> ans;
        for(int i = 0; i < k; i++)
        {
            ans.push_back(v[i].first);
        }
        return ans;
    }

};