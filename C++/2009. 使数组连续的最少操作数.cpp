//https://leetcode.cn/problems/minimum-number-of-operations-to-make-array-continuous/?envType=daily-question&envId=2024-04-08

class Solution {
public:
    int minOperations(vector<int>& nums) {
        int n_befor = nums.size();
        if(n_befor == 1) return 0; 
        sort(nums.begin(), nums.end());
        nums.resize(unique(nums.begin(), nums.end()) - nums.begin());//去重
        int n_after = nums.size(), ans = INT_MAX;
        for(int i = 0; i < n_after; i++)
        {
            int x = nums[i], y = x + n_befor - 1, left = i, right = n_after - 1;
            while(left < right)
            {
                int md = (left + right + 1) / 2;
                if(nums[md] > y) right = md - 1;
                else left = md; 
            }
            ans = min(ans, n_befor - (left - i + 1));
        }
        return ans;
    }
};