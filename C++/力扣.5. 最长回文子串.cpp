//https://leetcode.cn/problems/longest-palindromic-substring/description/


class Solution {
public:
    string longestPalindrome(string s) {
        int n = s.size(), len = 0;
        vector<vector<bool>> Ispalindrome(n, vector<bool>(n));
        string ans;
        for(int i = n - 1; i >= 0; i--)
        {
            for(int j = i; j < n; j++)
            {
                if(s[i] == s[j])
                {
                    if(i == j || i + 1 == j)
                    {
                        Ispalindrome[i][j] = true;
                    }
                    else
                    {
                        Ispalindrome[i][j] = Ispalindrome[i + 1][j - 1];
                    }
                }
                else
                {
                    Ispalindrome[i][j] = false;
                }
                if(Ispalindrome[i][j])
                {
                    int tmp = j - i + 1;
                    if(tmp > len)
                    {
                        ans = s.substr(i, tmp);
                        len = tmp;
                    }
                }
            }
        }
        return ans;
    }
};