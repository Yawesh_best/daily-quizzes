//https://leetcode.cn/problems/count-alternating-subarrays/description/?envType=daily-question&envId=2024-07-06https://leetcode.cn/problems/count-alternating-subarrays/description/?envType=daily-question&envId=2024-07-06

class Solution {
public:
    long long countAlternatingSubarrays(vector<int>& nums) {
        long long ans = 1;
        int n = nums.size(), left = 0;
        for(int i = 1; i < n; i++)
        {
            if(nums[i] == nums[i - 1]) left = i;
            ans += i - left + 1;
        }
        return ans;
    }
};