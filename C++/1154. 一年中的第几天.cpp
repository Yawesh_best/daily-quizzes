//https://leetcode.cn/problems/day-of-the-year/?envType=daily-question&envId=2023-12-31


class Solution {
public:
    int dayOfYear(string date) {
        int year = stoi(string(date.begin(), date.begin() + 4));
        int month = stoi(string(date.begin() + 5, date.begin() + 7));
        int day = stoi(string(date.end() - 2, date.end()));

        bool isLeapYear = (year % 400 == 0) || (year % 4 == 0 && year % 100 != 0);

        int daysOfMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        if (isLeapYear)
            daysOfMonth[1] = 29;

        int ans = 0;

        for (int i = 0; i < month - 1; ++i) {
            ans += daysOfMonth[i];
        }

        ans += day;

        return ans;
    }

};