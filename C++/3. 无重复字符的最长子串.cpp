//https://leetcode.cn/problems/longest-substring-without-repeating-characters/


class Solution
{
public:
    int lengthOfLongestSubstring(string s)
    {
        int hash[128] = { 0 }; // 使⽤数组来模拟哈希表
        int left = 0, right = 0, n = s.size();
        int ret = 0;
        while(right < n)
        {
            hash[s[right]]++; // 进⼊窗⼝
            while(hash[s[right]] > 1) // 判断
            hash[s[left++]]--; // 出窗⼝
            ret = max(ret, right - left + 1); // 更新结果
            right++; // 让下⼀个元素进⼊窗⼝
        }
    return ret;
    }
};