//https://leetcode.cn/problems/maximum-prime-difference/submissions/543598784/?envType=daily-question&envId=2024-07-02

class Solution {
public:
    int maximumPrimeDifference(vector<int>& nums) {
        bool primes[101];
        memset(primes, true, sizeof(primes));
        primes[0] = primes[1] = false;
        for (int i = 2; i <= 100; i++) {
            if (!primes[i]) continue;
            for (int j = 2; j * i <= 100; j++) {
                primes[j * i] = false;
            }
        }

        int mini = -1, maxi = -1;
        for (int i = 0; i < nums.size(); i++) {
            if (primes[nums[i]]) {
                if (mini == -1) {
                    mini = i;
                    if (maxi == -1) maxi = i;
                } else {
                    maxi = i;
                }
            }
        }

        return maxi - mini;
    }
};