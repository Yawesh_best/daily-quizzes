//https://leetcode.cn/problems/car-pooling/?envType=daily-question&envId=2023-12-02


class Solution {
public:
    bool carPooling(vector<vector<int>>& trips, int capacity) {
        int n = trips.size();
        unordered_map<int, int> form, to;
        for(int i = 0; i < n; i++)
        {
            form[trips[i][1]] += trips[i][0];
            to[trips[i][2]] += trips[i][0];
        }
        int j = 0;
        while(j < 1000 && capacity >= 0)
        {
            capacity += to[j];
            capacity -= form[j];
            if(capacity < 0) break;
            j++; 
        }
        return j == 1000;
    }
};