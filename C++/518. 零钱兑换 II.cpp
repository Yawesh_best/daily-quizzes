//https://leetcode.cn/problems/coin-change-ii/

class Solution {
public:
    int change(int amount, vector<int>& coins) {
        //dp[i][j]表⽰：从前 i 个硬币中挑选，总和正好等于 j ，存的值是一共有多少种选法
        int n = coins.size();
        vector<int> dp(amount + 1);
        dp[0] = 1;
        for(int i = 1; i <= n; i++)
            for(int j = coins[i - 1]; j <= amount; j++)
                dp[j] += dp[j - coins[i - 1]];
        return dp[amount];
    }
};