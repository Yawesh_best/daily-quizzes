//https://leetcode.cn/problems/maximum-binary-string-after-change/?envType=daily-question&envId=2024-04-10


#include <string>
#include <algorithm>
#include <iostream>

class Solution {
public:
    std::string maximumBinaryString(std::string& binary) {
        int c0 = 0, c1 = 0;
        for (char c : binary) 
        {
            c0 += ('1' - c);
            c1 += (c0 > 0) ? (c - '0') : 0;
        }
        if (c0 < 2) 
            return binary;
        else 
        {
            std::string result;
            result.resize(binary.length() - c1 - 1);
            std::fill_n(result.begin(), binary.length() - c1 - 1, '1');
            result += '0';
            for (int i = 0; i < c1; ++i)
                result += '1';
            return result;
        }
    }
};
