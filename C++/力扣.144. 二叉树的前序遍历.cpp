//https://leetcode.cn/problems/binary-tree-preorder-traversal/description/

class Solution {
public:

    //非递归迭代算法
    vector<int> preorderTraversal(TreeNode* root) {
        vector<int> v;
        stack<TreeNode*> tac;
        TreeNode* tmp = root;
        while(tmp || !tac.empty())
        {
            while(tmp)
            {
                v.push_back(tmp->val);
                tac.push(tmp);
                tmp = tmp->left;
            }
            TreeNode* cur = tac.top();
            tac.pop();
            tmp = cur->right;
        }
        return v;
    }
};