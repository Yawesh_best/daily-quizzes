//https://leetcode.cn/problems/binary-search-tree-to-greater-sum-tree/description/?envType=daily-question&envId=2023-12-04

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int findbig(TreeNode* root,int val) //返回一颗子树的值
    {
        if(root == nullptr)
            return 0;
        int res = 0;
        res += root->val;
        res += findbig(root->right, val);
        root->val = res + val;
        res += findbig(root->left, root->val);
        return res;
    }
    TreeNode* bstToGst(TreeNode* root) {
        findbig(root,0);
        return root;
    }
};