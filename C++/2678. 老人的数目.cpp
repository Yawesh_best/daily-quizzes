//https://leetcode.cn/problems/number-of-senior-citizens/?envType=daily-question&envId=2023-10-23

class Solution {
public:
    int countSeniors(vector<string>& details) {
        int count = 0;
        for(int i = 0; i < details.size(); i++)
        {
            if(stoi(string(details[i].end() - 4, details[i].end() - 2)) > 60) count++;;
        }
        return count;
    }
};