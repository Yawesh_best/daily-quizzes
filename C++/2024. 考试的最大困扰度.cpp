//https://leetcode.cn/problems/maximize-the-confusion-of-an-exam/description/?envType=daily-question&envId=2024-09-02

class Solution {
public:
    int window(string& Key, int k, char ch)
    {
        int n = Key.size(), ans = 0, left = 0, right = 0, sum = 0;
        while(right < n)
        {
            if(Key[right] != ch) sum++;
            while(sum > k)
            {
                if(Key[left] != ch) sum--;
                left++;
            }
            ans = max(ans, right - left + 1);
            right++;
        }
        return ans;
    }
    int maxConsecutiveAnswers(string answerKey, int k) {
        return max(window(answerKey, k, 'T'), window(answerKey, k, 'F'));
    }
};