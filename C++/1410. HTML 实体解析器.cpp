//https://leetcode.cn/problems/html-entity-parser/?envType=daily-question&envId=2023-11-23
class Solution {
public:
    string entityParser(string str) {
        unordered_map<string, char> entityToChar= {
            {"&quot;",'\"'},
            {"&apos;",'\''},
            {"&amp;",'&'},
            {"&gt;",'>'},
            {"&lt;",'<'},
            {"&frasl;",'/'}
        };

        string ans;
        for (int i = 0; i < str.length(); i++) {
            if (str[i] == '&') {
                int j = i + 1;
                while (j < str.length() && str[j] != ';') {
                    if(str[j] == '&')
                    {
                        string tmp = str.substr(i, j - i);
                        ans += tmp;
                        i = j;
                    }
                    j++;
                }
                if (j < str.length()) {
                    string entity = str.substr(i, j - i + 1);
                    auto it = entityToChar.find(entity);
                    if (it != entityToChar.end()) {
                        ans.push_back(it->second);
                    } else {
                        ans += entity;
                    }
                    i = j;
                } else {
                    ans.push_back(str[i]);
                }
            } else {
                ans.push_back(str[i]);
            }
        }
        return ans;
    }
};