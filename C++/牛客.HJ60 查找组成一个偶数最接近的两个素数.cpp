//https://www.nowcoder.com/practice/f8538f9ae3f1484fb137789dec6eedb9?tpId=37&&tqId=21283&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking


#include <iostream>
#include <ostream>
using namespace std;
bool isprime(int n)
{
    int tmp = n / 2;
    for(int i = 2; i < tmp; i++)
    {
        if(n % i == 0) return false;

    }
    return true;
}
int main() {
    int n;
    cin>>n;
    for(int i = n / 2; i > 0; i--)
    {
        if(isprime(i) && isprime(n - i))
        {
            cout<<i<<endl<<n - i<<endl;
            break;
        }
    }    
    return 0;
}
