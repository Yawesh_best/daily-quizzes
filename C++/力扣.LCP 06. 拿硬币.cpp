//https://leetcode.cn/problems/na-ying-bi/?envType=daily-question&envId=2023-09-20


class Solution {
public:
    int minCount(vector<int>& coins) {
        int ans = 0;
        for(int coin : coins)
        {
            ans += (coin - 1) / 2 + 1;
        }
        return ans;
    }
};