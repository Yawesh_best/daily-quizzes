//https://leetcode.cn/problems/harshad-number/?envType=daily-question&envId=2024-07-03


class Solution {
public:
    int sum(int n)
    {
        int ans = 0;
        while(n)
        {
            ans += n % 10;
            n /= 10;
        }
        return ans;
    }
    int sumOfTheDigitsOfHarshadNumber(int x) {
        int num = sum(x);
        return x % num == 0 ? num : -1;
    }
};