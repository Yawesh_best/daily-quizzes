//https://leetcode.cn/problems/wildcard-matching/


class Solution {
public:
    bool isMatch(string s, string p) 
    {
        int m = s.size(), n = p.size();
        s = " " + s, p = " " + p;
        vector<vector<bool>> dp(m + 1, vector<bool>(n + 1)); // 1. 创建 dp 表
        dp[0][0] = true; // 2. 初始化
        for(int j = 1; j <= n; j++)
            if(p[j] == '*') dp[0][j] = true;
            else break;

        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(p[j] == '*')
                    dp[i][j] = dp[i - 1][j] || dp[i][j - 1];
                else
                    dp[i][j] = (p[j] == '?' || s[i] == p[j]) && dp[i - 1][j -1];
            }
        }
        // 4. 返回值
        return dp[m][n];
    }
};