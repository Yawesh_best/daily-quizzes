//https://www.nowcoder.com/practice/4b1658fd8ffb4217bc3b7e85a38cfaf2?tpId=37&tqId=21309&ru=/exam/oj


#include <iostream>
using namespace std;

int main() {
    int n, count, maxcount;
    cin >> n;
    while(n)
    {
        if(n & 1) count++;
        else count = 0;
        maxcount = max(maxcount, count);
        n = n >> 1;
    }
    cout<< maxcount;
    return 0;
}