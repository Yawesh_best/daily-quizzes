//https://leetcode.cn/problems/high-access-employees/


class Solution {
    int ask(const string &s)
    {
        return (s[0] - '0')*600 + (s[1] - '0')*60 + (s[2] - '0')*10 + (s[3] - '0');
    }
public:
    vector<string> findHighAccessEmployees(vector<vector<string>>& access_times) {
        map<string,vector<int>> m;
        for(auto &c:access_times)m[c[0]].push_back(ask(c[1]));
        vector<string> ans;
        for(auto &p:m)
        {
            sort(p.second.begin(),p.second.end());
            int l=p.second.size(),i;
            for(i=0;i+2<l;i++)if(p.second[i+2]-p.second[i]<60)break;
            if(i+2<l)ans.push_back(p.first);
        }
        return ans;
    }
};