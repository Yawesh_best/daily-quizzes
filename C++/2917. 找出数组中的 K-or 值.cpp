//https://leetcode.cn/problems/find-the-k-or-of-an-array/?envType=daily-question&envId=2024-03-06




class Solution {
public:
    int findKOr(vector<int>& nums, int k) {
        int res = 0;
        for (int i = 0; i < 31; i++) { // 最大为 2 ^ 31 - 1，即二进制右起向左最多31位
            int cnt = 0; // 数组中第 i 位为 1 的数计数
            for (int n : nums) cnt += (n >> i) & 1; // 遍历数组，n >> i 为 n 二进制右移动 i 位，对 1 按位与若为 1 则该位为 1，否则为 0
            res |= cnt >= k ? (1 << i) : 0; // 若 cnt 超过 k，res 第 i 位置 1（1 左移 i 位后按位或），否则保持不变
        }
        return res;
    }
};