//https://leetcode.cn/problems/maximum-count-of-positive-integer-and-negative-integer/description/?envType=daily-question&envId=2024-04-09https://leetcode.cn/problems/maximum-count-of-positive-integer-and-negative-integer/description/?envType=daily-question&envId=2024-04-09

class Solution {
public:
    int maximumCount(vector<int>& nums) {
        for(int i = 0; i < nums.size(); i++)
        {
            if(nums[i] > 0) return max((int)nums.size() - i, i);
            else if(nums[i] == 0)
            {
                int j = i;
                while(j < nums.size() && nums[j] == 0) j++;
                return max(i, (int)nums.size() - j);
            }
             
        }
        return nums.size();
    }
};