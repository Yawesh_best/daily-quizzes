//https://leetcode.cn/problems/number-of-ways-to-buy-pens-and-pencils/description/?envType=daily-question&envId=2023-09-01


class Solution {
public:
    long long waysToBuyPensPencils(int total, int cost1, int cost2) 
    {
        
        long res = 0, cnt = 0;
        while (cnt * cost1 <= total) 
        {
            res += (total - cnt * cost1) / cost2 + 1;
            cnt++;
        }
        return res;
    }
};