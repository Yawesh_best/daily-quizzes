//https://leetcode.cn/problems/single-number-ii/?envType=daily-question&envId=2023-10-15


class Solution {
public:
    int singleNumber(vector<int>& nums) {
        map<int, int> count;
        for (int n : nums) 
        {
            count[n]++;
        }
        for (auto it : count) 
        {
            if (it.second == 1) 
            {
                return it.first;
            }
        }
        return 0;
    }
};
