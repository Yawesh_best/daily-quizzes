//https://leetcode.cn/problems/sum-of-digit-differences-of-all-pairs/?envType=daily-question&envId=2024-08-30

class Solution {
public:
    long long sumDigitDifferences(vector<int>& nums) {
        string s = to_string(nums[0]);
        int L = s.length(), n = nums.size();
        long long ans = 0;
        for(int i = 0; i < L; i++){
            unordered_map<string,int> mp;
            for(int x : nums){
                string zhi = to_string(x);
                mp[zhi.substr(i,1)]++;
            }
            int val = n;
            for(auto [x,y] : mp){
                ans += 1ll * y * (val - y);
                val -= y;
            }
        }
        return ans;
    }
};