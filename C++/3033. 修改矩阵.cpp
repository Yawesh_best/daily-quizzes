//https://leetcode.cn/problems/modify-the-matrix/submissions/544266764/?envType=daily-question&envId=2024-07-05https://leetcode.cn/problems/modify-the-matrix/submissions/544266764/?envType=daily-question&envId=2024-07-05


class Solution {
public:
    vector<vector<int>> modifiedMatrix(vector<vector<int>>& matrix) {
        int m = matrix.size(), n = matrix[0].size();
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(matrix[i][j] == -1)
                {
                    int ma = -1;
                    // for(int k = 0; k < n; k++) ma = max(ma, matrix[i][k]);
                    for(int a = 0; a < m; a++) ma = max(ma, matrix[a][j]);
                    matrix[i][j] = ma;
                }
            }
        }
        return matrix;
    }
};