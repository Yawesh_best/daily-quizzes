//https://leetcode.cn/problems/number-of-longest-increasing-subsequence/description/


class Solution {  
public:
    int findNumberOfLIS(vector<int>& nums) {
        int n = nums.size(), lon = 1, ans = 0;
        vector<int> len(n, 1), count(n, 1);
        for(int i = 1; i < n; i++)
        {
            for(int j = 0; j < i; j++)
            {
                if(nums[j] < nums[i])
                {
                    if(len[j] + 1 == len[i])
                    {
                        count[i] += count[j];
                    }
                    else if(len[j] + 1 > len[i])
                    {
                        len[i] = len[j] + 1;
                        count[i] = count[j]; 
                    }
                }
            }
            lon = max(lon, len[i]);
        }
        for(int i = 0; i < n; i++)
        {
            if(len[i] == lon)
            {
                ans += count[i];
            }
        }
        return ans;
    }
};