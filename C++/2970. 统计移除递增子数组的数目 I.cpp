//https://leetcode.cn/problems/count-the-number-of-incremovable-subarrays-i/description/?envType=daily-question&envId=2024-07-10https://leetcode.cn/problems/count-the-number-of-incremovable-subarrays-i/description/?envType=daily-question&envId=2024-07-10

class Solution {
public:
    int incremovableSubarrayCount(vector<int>& nums) {
        int n = nums.size();
        int ans = 0;
        for(int l = 0; l < n; l++) {
            for(int r = l; r < n; r++) {
                vector<int> temp;
                for(int i = 0; i < l; i++) temp.push_back(nums[i]);
                for(int i = r + 1; i < n; i++) temp.push_back(nums[i]);
                int m = temp.size();
                int delta = 1;
                for(int i = 1; i < m; i++) {
                    if(temp[i - 1] >= temp[i]) {
                        delta = 0;
                        break;
                    }
                }
                ans += delta;
            }
        }
        return ans;
    }
};