//https://leetcode.cn/problems/minimum-path-cost-in-a-grid/?envType=daily-question&envId=2023-11-22



class Solution {
public:
    int minPathCost(vector<vector<int>>& grid, vector<vector<int>>& moveCost) {
        int m=grid.size();
        int n=grid[0].size();
        // 定义dp[i][j]为到达ij的最小cost
        int dp[m][n];
        memset(dp,0x3f3f3f3f,sizeof dp);
        // 初始化边界条件
        for(int i=0;i<n;i++){
            dp[0][i]=grid[0][i]; 
        }
        int ans=0x3f3f3f3f;
        for(int i=1;i<m;i++){
            for(int j=0;j<n;j++){
                for(int z=0;z<n;z++){
                    dp[i][j]=min(dp[i][j],dp[i-1][z]+moveCost[grid[i-1][z]][j]+grid[i][j]);
                }
            }
        }
        for(int i=0;i<n;i++){
            ans=min(ans,dp[m-1][i]);
        }
        return ans;
    }
};