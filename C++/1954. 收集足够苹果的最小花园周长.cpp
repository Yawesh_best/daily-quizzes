//https://leetcode.cn/problems/minimum-garden-perimeter-to-collect-enough-apples/description/?envType=daily-question&envId=2023-12-24



class Solution {
public:
    long long minimumPerimeter(long long neededApples) {
        long long target;
        for(long long i = 1; ;i++)
        {
            target = 2 * (2 * i + 1) * i * ( i + 1);
            if(target >= neededApples)
            {
                return i * 8;
            }
        }
    }
};