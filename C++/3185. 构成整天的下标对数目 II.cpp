//https://leetcode.cn/problems/count-pairs-that-form-a-complete-day-ii/description/

class Solution {
public:
    long long countCompleteDayPairs(vector<int>& hours) {
        unordered_map<int, int> mp;
        long long cnt = 0;
        for(const auto& num: hours) {
            if(num % 24 == 0) cnt += mp[0];
            else if(mp[24 - (num % 24)]) cnt += mp[24 - (num % 24)];
            mp[num % 24]++;
        }
        return cnt;
    }
};