//https://leetcode.cn/problems/count-common-words-with-one-occurrence/description/?envType=daily-question&envId=2024-01-12



class Solution {
public:
    int countWords(vector<string>& words1, vector<string>& words2) {
        int ans = 0;
        unordered_map<string, int> ma1, ma2;
        for(auto st : words1)
        {
            ma1[st]++;
        }
        for(auto s : words2)
        {
            ma2[s]++;
        }
        for(auto str : ma1)
        {
            if(str.second == 1 && ma2[str.first] == 1) ans++;
        }
        return ans;
    }
};
