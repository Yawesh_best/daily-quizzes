//https://leetcode.cn/problems/find-the-power-of-k-size-subarrays-i/?envType=daily-question&envId=2024-11-06


class Solution {
public:
    vector<int> resultsArray(vector<int>& nums, int k) {
        int n = nums.size();
        if(k == 1) return nums;
        if(k == n)
        {
            for(int i = 1; i < n; i++)
            {
                if(nums[i - 1] != nums[i] - 1) return {-1};
                else return {nums[n - 1]}; 
            }
        }
        vector<int> ans(n - k + 1, -1);
        for(int i = 0; i < n - k + 1; i++)
        {
            bool istrue = true;
            for(int j = i + 1; j < i + k; j++)
            {
                if(nums[j] != nums[j - 1] + 1)
                {
                    istrue = false;
                    break;
                }
            }
            if(istrue)
            {
                ans[i] = nums[i + k - 1];
            }
        }
        return ans;
    }
};