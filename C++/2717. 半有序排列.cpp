//https://leetcode.cn/problems/semi-ordered-permutation/?envType=daily-question&envId=2024-12-11

class Solution {
public:
    int semiOrderedPermutation(vector<int>& nums) {
        int n = nums.size(), num1 = -1, numn = -1, ans = 0;
        for(int i = 0; i < n; i++)
        {
            if(nums[i] == 1) num1 = i;
            if(nums[i] == n) numn = i;
            if(numn != -1 && num1 != -1) break;
        }
        if(numn > num1) ans = num1 + n - numn -1;
        else ans = num1 + n - numn -2;
        return ans;
    }
};