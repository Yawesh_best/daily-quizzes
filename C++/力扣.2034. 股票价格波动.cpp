//https://leetcode.cn/problems/stock-price-fluctuation/?envType=daily-question&envId=2023-10-08


class StockPrice {
public:
    map<int,int> kv;//<key,val>
    map<int,int> vs;//<val,size>
    StockPrice() {

    }

    void update(int timestamp, int price) 
    {
        if (kv.find(timestamp) != kv.end())
        {
            --vs[kv[timestamp]];
            if (!vs[kv[timestamp]])vs.erase(kv[timestamp]);
        }
        kv[timestamp] = price;
        ++vs[price];
    }

    int current()
    {
        return kv.rbegin()->second;
    }

    int maximum()
    {
        return vs.rbegin()->first;
    }

    int minimum()
    {
        return vs.begin()->first;
    }
};