//https://leetcode.cn/problems/minimum-number-of-days-to-eat-n-oranges/description/?envType=daily-question&envId=2024-05-12



class Solution { 
    map<int, int> memo{};
public:
    int minDays(int n) {
        if(n < 2) return n;
        if(memo[n]) return memo[n];
        // 除法总比减法效率更高。优先进行除法运算

        int t2 = minDays(n / 2) + (n % 2); // n % 2 是吃到偶数的天数
        int t3 = minDays(n / 3) + (n % 3); // n % 3 是吃到3倍数的天数

        memo[n] = min(t2, t3) + 1;
        return memo[n];
    } 
};