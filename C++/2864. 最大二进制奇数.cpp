//https://leetcode.cn/problems/maximum-odd-binary-number/submissions/511416652/?envType=daily-question&envId=2024-03-13


class Solution {
public:
    string maximumOddBinaryNumber(string s) {
        int count = 0;
        int n = s.size();
        for(int i = 0;i<n;++i)
        {
            if(s[i] == '1')
            { 
                s[i] = '0';
                count++;
            }
        }
        s[n-1] = '1';
        count--;
        for(int i = 0;i<n-1;++i)
        {
            if(count)
            {
                s[i] = '1';
                count--;
            }
        }
        return s;
    }
};