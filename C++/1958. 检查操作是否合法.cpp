//https://leetcode.cn/problems/check-if-move-is-legal/description/?envType=daily-question&envId=2024-07-07https://leetcode.cn/problems/check-if-move-is-legal/description/?envType=daily-question&envId=2024-07-07


class Solution {
    bool isGoodLine(vector<vector<char>>& board, int startX, int startY, int stepX, int stepY, char color) {
        char target = color == 'W' ? 'B' : 'W';
        auto isValid = [&board](int x, int y) -> bool {
            if (x >= 0 && x < board.size() && y >= 0 && y < board[0].size()) {
                return true;
            } else { return false; }
        };

        int steps = 1;

        while (isValid(startX + stepX, startY + stepY)) {
            int x = startX + stepX,
                y = startY + stepY;
            steps++;
            
            if (board[x][y] == target) {
                startX = x, startY = y;
            } else {
                return board[x][y] == color && steps > 2;
            }
        }

        return false;
    }
public:
    bool checkMove(vector<vector<char>>& board, int rMove, int cMove, char color) {
        mutex mtx;
        bool res = false;

        auto task = [&board, &rMove, &cMove, &color, &res, &mtx, this](int dx, int dy) {
            lock_guard<mutex> locker(mtx);
            res |= isGoodLine(board, rMove, cMove, dx, dy, color);
        };

        auto t1 = thread(task, 1, 0);
        auto t2 = thread(task, -1, 0);
        auto t3 = thread(task, 0, 1);
        auto t4 = thread(task, 0, -1);
        auto t5 = thread(task, 1, 1);
        auto t6 = thread(task, 1, -1);
        auto t7 = thread(task, -1, -1);
        auto t8 = thread(task, -1, 1);

        t1.join();
        t2.join();
        t3.join();
        t4.join();
        t5.join();
        t6.join();
        t7.join();
        t8.join();

        return res;
    }
};