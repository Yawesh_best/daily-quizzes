//https://leetcode.cn/problems/first-completely-painted-row-or-column/?envType=daily-question&envId=2023-12-01

class Solution {
public:
    int firstCompleteIndex(vector<int>& arr, vector<vector<int>>& mat) {
        int m = mat.size(), n = mat[0].size(), ans = arr.size();
        unordered_map<int, int> ma;
        for(int i = 0; i < arr.size(); i++)
            ma[arr[i]] = i;
        vector<int> rows(m),cols(n);
        for(int i = 0; i < m; i++)
        {
            for(int j = 0; j < n; j++)
            {
                rows[i]=max(rows[i],ma[mat[i][j]]);
                cols[j]=max(cols[j],ma[mat[i][j]]);
            }
        }
        for(int &row:rows) ans=min(ans,row);
        for(int &col:cols) ans=min(ans,col);
        return ans;
    }
};