//https://leetcode.cn/problems/single-number/?envType=daily-question&envId=2023-10-14


class Solution {
public:
    int singleNumber(vector<int>& nums) {
        int ans = 0;
        for(auto e : nums)
        {
            ans ^= e;
        }
        return ans;
    }
};