//https://leetcode.cn/problems/construct-binary-tree-from-preorder-and-inorder-traversal/?envType=daily-question&envId=2024-02-20https://leetcode.cn/problems/construct-binary-tree-from-preorder-and-inorder-traversal/?envType=daily-question&envId=2024-02-20


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* _buildTree(vector<int>& preorder, vector<int>& inorder, int& prei, int inbegin, int inend)
    {
        if(inbegin > inend) return nullptr;
        TreeNode* node = new TreeNode(preorder[prei]);
        int inoi = inbegin;
        while(inoi <= inend)
        {
            if(inorder[inoi] == preorder[prei])
            {
                break;
            }
            else
                inoi++;
        }
        prei++;
        node->left = _buildTree(preorder, inorder, prei,inbegin, inoi - 1);

        node->right = _buildTree(preorder, inorder, prei, inoi + 1, inend);
        return node;
    }


    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) 
    {
        int i = 0;
        return _buildTree(preorder, inorder, i, 0, inorder.size() - 1);
    }
};