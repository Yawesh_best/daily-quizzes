//https://leetcode.cn/problems/maximum-difference-between-node-and-ancestor/?envType=daily-question&envId=2024-04-05

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int dfs(TreeNode* node, int ma, int mi)
    {
        if(node == nullptr) return(ma - mi);
        ma = max(ma, node->val);
        mi = min(mi, node->val);
        return max(dfs(node->left, ma, mi), dfs(node->right, ma, mi));
    }
    int maxAncestorDiff(TreeNode* root) {
        return dfs(root, root->val, root->val);
    }
};