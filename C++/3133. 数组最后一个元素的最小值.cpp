//https://leetcode.cn/problems/minimum-array-end/description/?envType=daily-question&envId=2024-08-22

class Solution {
    public :
    long long minEnd(int n, int x) {
        int k=n-1;
        int p1=0;int p2=0;
        long long r=x;
        while((k>>p2)!=0){
            if(((r>>p1)&1L)==0){
                r=r|(((k>>p2)&1L)<<p1);
                p2++;
            } 
            p1++;
        }
        return r;
    }
};