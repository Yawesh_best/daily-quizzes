//https://leetcode.cn/problems/points-that-intersect-with-cars/description/?envType=daily-question&envId=2024-09-15


class Solution {
public:
    int numberOfPoints(vector<vector<int>>& nums) {
        sort(nums.begin(), nums.end(), 
        [](const std::vector<int>& a, const std::vector<int>& b) 
        {  
            return a[0] < b[0]; // 根据每行的第一个元素进行升序排序  
        });
        int ans = 0, n = nums.size(), left = nums[0][0], right = nums[0][1];
        for(int i = 1; i < n; i++)
        {
            if(nums[i][0] > right)
            {
                ans += (right - left + 1);
                left = nums[i][0];
                right = nums[i][1];
            }
            else if(nums[i][1] > right)
            {
                right = nums[i][1];
            }
        }
        ans += (right - left + 1);
        return ans;
    }
};