//https://leetcode.cn/problems/remove-nodes-from-linked-list/?envType=daily-question&envId=2024-01-03


/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* removeNodes(ListNode* head) {
        if(head == nullptr || head->next == nullptr) return head;
        head->next = removeNodes(head->next);
        if(head->val < head->next->val) return head->next;
        return head;
    }
};

