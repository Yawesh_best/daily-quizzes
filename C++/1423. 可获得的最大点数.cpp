//https://leetcode.cn/problems/maximum-points-you-can-obtain-from-cards/description/?envType=daily-question&envId=2023-12-03


class Solution {
public:
    int maxScore(vector<int>& cardPoints, int k) {
        int n = cardPoints.size();
        int l = 0, r = n - k, ans = 0;
        for(int j = 0; j < n; j++)
            ans += cardPoints[j];
        for(int i = 0; i <= r - 1; i++)
            ans -= cardPoints[i];
        int maxans = ans;
        while(r <= n - 1)
        {
            ans += cardPoints[l++];
            ans -= cardPoints[r++];
            maxans = max(ans, maxans);
        }
        return maxans;
    }

};