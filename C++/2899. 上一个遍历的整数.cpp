//https://leetcode.cn/problems/last-visited-integers/


class Solution {
public:
    vector<int> lastVisitedIntegers(vector<string>& words) {
        stack<int> sta1, sta2;
        vector<int> ans;
        for(int i = 0; i < words.size(); i++)
        {
            if(words[i][0] == 'p')
            {
                if(sta2.empty())
                {
                    ans.push_back(-1);
                }
                else
                {
                    ans.push_back(sta2.top());
                    sta2.pop();
                }
            }
            else
            {
                sta1.push(stoi(words[i]));
                sta2 = sta1;
            }
        }
        return ans;
    }
};