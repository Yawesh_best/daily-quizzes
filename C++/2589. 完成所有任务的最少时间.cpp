//https://leetcode.cn/problems/minimum-time-to-complete-all-tasks/submissions/532240942/?envType=daily-question&envId=2024-05-15

class Solution {
public:  // 暴力 + 贪心
    int findMinimumTime(vector<vector<int>>& tasks) {
        int n = tasks.size();

        // 按结束时间排序
        sort(begin(tasks), end(tasks), [&](vector<int> &a, vector<int> &b){
            if(a[1] == b[1]) return a[0] < b[0];
            return a[1] < b[1];
        });

        // 贪心, 每个任务都尽量拖延，迟点做
        map<int,int> list; // 记录哪些时间点开始做任务了 
        
        for(auto & task: tasks){
            // 先看看区间内是否存在已经开始做任务的时间点
            for(int i = task[0]; i <= task[1] && task[2] > 0; i++){
                if(list.count(i)) task[2]--;
            }
            // 如果区间内已经不存在已经开始做任务的时间节点，且还有任务没完成，就从后往前开
            int i = task[1]; 
            while(i >= task[0] && task[2] > 0){
                if(!list.count(i)){
                    task[2]--;
                    list[i]++;
                }
                i--;
            }
        }

        return list.size();
    }
};