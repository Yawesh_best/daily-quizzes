//https://leetcode.cn/problems/maximum-length-of-subarray-with-positive-product/description/

class Solution {
public:
    int getMaxLen(vector<int>& nums) {
        int n = nums.size(), ans = 0;
        vector<int> f(n + 1), g(n + 1);
        for(int i = 1; i <= n; i++)
        {
            if(nums[i - 1] > 0)
            {
                f[i] = f[i - 1] + 1;
                g[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
            }
            else if(nums[i - 1] == 0)
            {
                f[i] = g[i] = 0;
            }
            else
            {
                f[i] = g[i - 1] == 0 ? 0 : g[i - 1] + 1;
                g[i] = f[i - 1] + 1;
            }
            ans = max(ans, f[i]);
        }
        return ans;


    }
};