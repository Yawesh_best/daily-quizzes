//https://leetcode.cn/problems/design-hashmap/description/?envType=daily-question&envId=2024-04-15

class MyHashMap {
public:

    MyHashMap() {
        hash = vector<int> (1e6 + 1, -1);
    }
    
    void put(int key, int value) {
        hash[key] = value;
    }
    
    int get(int key) {
        return hash[key];
    }
    
    void remove(int key) {
        hash[key] = -1;
    }
private:
    vector<int> hash;
};

/**
 * Your MyHashMap object will be instantiated and called as such:
 * MyHashMap* obj = new MyHashMap();
 * obj->put(key,value);
 * int param_2 = obj->get(key);
 * obj->remove(key);
 */