//https://leetcode.cn/problems/bulls-and-cows/description/?envType=daily-question&envId=2024-03-10https://leetcode.cn/problems/bulls-and-cows/description/?envType=daily-question&envId=2024-03-10

class Solution {
public:
    string getHint(string secret, string guess) {
        int n = secret.size();
        unordered_map<char, int> se, gu;
        int Bulls = 0, Cows = 0;
        for (int i = 0; i < n; i++) {
            if (secret[i] == guess[i]) {
                Bulls++;
            } else {
                if (se.find(guess[i]) != se.end() && se[guess[i]] > 0) {
                    Cows++;
                    se[guess[i]]--;
                } else {
                    gu[guess[i]]++;
                }
                if (gu.find(secret[i]) != gu.end() && gu[secret[i]] > 0) {
                    Cows++;
                    gu[secret[i]]--;
                } else {
                    se[secret[i]]++;
                }
            }
        }
        return to_string(Bulls) + 'A' + to_string(Cows) + 'B';
    }
};