//https://leetcode.cn/problems/longest-even-odd-subarray-with-threshold/description/?envType=daily-question&envId=2023-11-16



class Solution {
public:
    int longestAlternatingSubarray(vector<int>& nums, int threshold) {
        int n = nums.size(), res = 0;
        int l = 0, r = 0;
        while(l < n) 
        {
            while(l < n && (nums[l] % 2 || nums[l] > threshold)) l++;
            if(l == n) break;
            r = l + 1;
            while(r < n && (nums[r] % 2 != nums[r - 1] % 2 && nums[r] <= threshold)) r++;
            res = max(r - l, res);
            l = r; 
        }
        return res;
    }
};