//https://www.nowcoder.com/practice/e896d0f82f1246a3aa7b232ce38029d4?tpId=37&&tqId=21282&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking

#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;


int main() {
    string str;
    while (cin >> str) 
    { 
        char ans = -1;
        unordered_map<char, int> ma;
        for(auto arr : str)
        {
            ma[arr]++;
        }
        for(auto e : str)
        {
            if(ma[e] == 1)
            {
                ans = e;
                break;
            }
        }
        
        if(ans == -1)
            cout<< -1 <<endl;
        else
            cout<< ans <<endl;
    }
}