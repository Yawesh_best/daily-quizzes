//https://leetcode.cn/problems/student-attendance-record-i/description/?envType=daily-question&envId=2024-08-18

class Solution {
public:
    bool checkRecord(string s) {
        return !(std::count(s.begin(), s.end(), 'A') >= 2 || s.find("LLL") != -1);
    }
};