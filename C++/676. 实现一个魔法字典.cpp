//https://leetcode.cn/problems/implement-magic-dictionary/description/?envType=daily-question&envId=2024-08-12

class MagicDictionary {
public:
    vector<string> dictionary_m;
    MagicDictionary() {
    }
    
    void buildDict(vector<string> dictionary) {
        dictionary_m = dictionary;
    }
    
    bool search(string searchWord) {
        for(auto s : dictionary_m)
        {
            if(s.size() != searchWord.size())
            {
                continue;
            }
            int tmp = 0;
            for(int i = 0; i < s.size(); i++)
            {
                if(s[i] != searchWord[i])
                    tmp++;
            }
            if(tmp == 1) return true;
        }
        return false;
    }
};

/**
 * Your MagicDictionary object will be instantiated and called as such:
 * MagicDictionary* obj = new MagicDictionary();
 * obj->buildDict(dictionary);
 * bool param_2 = obj->search(searchWord);
 */