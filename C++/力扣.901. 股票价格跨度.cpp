//https://leetcode.cn/problems/online-stock-span/?envType=daily-question&envId=2023-10-07

class StockSpanner {
public:
    
    StockSpanner() 
    {
        lt.push_back(make_pair(INT_MAX,1));
    }

    int next(int price) 
    {
        int num = 1;
        while (price >= lt.back().first)
        {
            num += lt.back().second;
            lt.pop_back();
        }
        lt.push_back(make_pair(price,num));
        return num;
    }
private:
    vector<pair<int,int>> lt;
};