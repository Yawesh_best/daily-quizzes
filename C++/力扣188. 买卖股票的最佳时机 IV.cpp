//https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-iv/description/

class Solution {
public:
    int maxProfit(int k, vector<int>& prices) {
        int n = prices.size();
        int INF = 0x3f3f3f;
        vector<vector<int>> f(n, (vector<int>((k + 1), (-INF))));
        auto g = f;
        f[0][0] = -prices[0], g[0][0] = 0;
        for(int i = 1; i < n; i++)
        {
            for(int j = 0; j <= k; j++)
            {
                f[i][j] = max(f[i - 1][j], g[i - 1][j] - prices[i]);
                g[i][j] = g[i - 1][j];
                if(j > 0)
                {
                    g[i][j] = max(g[i - 1][j], f[i - 1][j - 1] + prices[i]);
                }
            }
        }
        int ans = g[n - 1][0];
        for(int i = 1; i <= k; i++)
        {
            ans = max(ans, g[n - 1][i]);
        }
        return ans;
    }
};