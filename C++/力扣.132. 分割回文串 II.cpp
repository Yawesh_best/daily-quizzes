//https://leetcode.cn/problems/palindrome-partitioning-ii/description/

class Solution {
public:
    int minCut(string s) {
        int n = s.size();
        vector<vector<bool>> Issubsequence(n, vector<bool>(n));
        for(int i = n - 1; i >= 0; --i)
            for(int j = i; j < n; j++)
                if(s[i] == s[j])                    
                    Issubsequence[i][j] = i + 1 < j ? Issubsequence[i + 1][j - 1] : true;
        vector<int> dp(n, INT_MAX - 10);
        for(int i = 0; i < n; ++i)
        {
            if(Issubsequence[0][i]) dp[i] = 0;
            for(int j = 1; j <= i; j++)
            {
                if(Issubsequence[j][i])
                {
                    dp[i] = min(dp[j - 1] + 1, dp[i]);
                }
            }
        }
        return dp[n - 1];
    }
};