//https://www.nowcoder.com/practice/e2a22f0305eb4f2f9846e7d644dba09b?tpId=37&&tqId=21314&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking


#include <iostream>
using namespace std;
#include <vector>
int main() {
    int n, m;
    cin >> n >> m;
    vector<vector<int>> dp(n + 2, vector<int> (m + 2));
    dp[0][1] = 1;
    for (int i = 1; i <= n + 1; i++) {
        for (int j = 1; j <= m + 1; j++) {
            dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
        }
    }
    cout << dp[n + 1][m + 1];
}