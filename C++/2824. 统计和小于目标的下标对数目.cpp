//https://leetcode.cn/problems/count-pairs-whose-sum-is-less-than-target/?envType=daily-question&envId=2023-11-24

class Solution {
public:
    int countPairs(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        int left = 0, right = nums.size() - 1, ans = 0;
        while (left < right) 
        {
            if (nums[left] + nums[right] < target) 
            {
                ans += right - left;
                left++;
            } 
            else 
                right--;
        }
        return ans;
    }
};
