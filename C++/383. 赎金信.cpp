//https://leetcode.cn/problems/ransom-note/description/?envType=daily-question&envId=2024-01-07


class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        int map[26] = {0};
        for(const auto& c: magazine) {
            map[c - 'a']++;
        }
        for(const auto& c: ransomNote) {
            if(map[c - 'a'] == 0) return false;
            map[c - 'a']--;
        }
        return true;
    }
};