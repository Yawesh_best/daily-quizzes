//https://leetcode.cn/problems/max-consecutive-ones-iii/description/


int ret = 0;
for(int left = 0, right = 0, zero = 0; right < nums.size(); right++)
{
if(nums[right] == 0) zero++; // 进窗⼝
while(zero > k) // 判断
if(nums[left++] == 0) zero--; // 出窗⼝
ret = max(ret, right - left + 1); // 更新结果
}
return ret;