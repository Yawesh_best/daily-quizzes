//https://leetcode.cn/problems/subarrays-distinct-element-sum-of-squares-i/description/


class Solution {
public:
  int sumCounts(vector<int>& nums) {
    int n = nums.size();
    int ans = 0;
    for (int i = 0; i < n; i++) 
    {
      set<int> S;
      for (int j = i; j < n; j++) 
      {
          S.insert(nums[j]);
          ans+=S.size()*S.size();
      }
    }
    return ans;
  }
};