//https://leetcode.cn/problems/divide-array-into-arrays-with-max-difference/



class Solution {
public:
    vector<vector<int>> divideArray(vector<int>& nums, int k) {
        int n = nums.size();
        sort(nums.begin(), nums.end());
        vector<vector<int>> ans, tmp;
        for(int i = 2; i < n; i+=3)
        {
            if(nums[i] - nums[i - 2] > k) return tmp;
            vector<int> subarray = {nums[i - 2], nums[i - 1], nums[i]};
            ans.push_back(subarray);
        }
        return ans;
    }
};