//https://leetcode.cn/problems/check-if-there-is-a-valid-partition-for-the-array/description/?envType=daily-question&envId=2024-03-01


class Solution {
public:
    bool validPartition(vector<int>& nums) {
        int n = nums.size();
        if (n == 2) {
            return nums[1] == nums[0];
        }
        vector<int> dp(n + 1, 0);
        dp[0] = 1;
        dp[2] = nums[1] == nums[0];
        for (int i = 3; i < n + 1; ++i) {
            if (nums[i - 1] == nums[i - 2]) {
                dp[i] |= dp[i - 2];
            }
            if (nums[i - 1] == nums[i - 2] && nums[i - 2] == nums[i - 3]) {
                dp[i] |= dp[i - 3];
            }
            if (nums[i - 1] - nums[i - 2] == 1 && nums[i - 2] - nums[i - 3] == 1) {
                dp[i] |= dp[i - 3];
            }
        }
        return dp[n];
    }
};