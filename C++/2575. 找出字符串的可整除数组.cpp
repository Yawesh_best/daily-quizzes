//https://leetcode.cn/problems/find-the-divisibility-array-of-a-string/?envType=daily-question&envId=2024-03-07


class Solution {
public:
    vector<int> divisibilityArray(string word, int m) {
        int len = word.size();
        vector<int> res(len,0);
        long long cur = 0;
        for(int i = 0;i<len;i++){
            cur+=(word[i]-'0');
            cur%=m;
            if(cur==0){
                res[i]=1;
            }
            cur*=10;
        }
        return res;
    }
};