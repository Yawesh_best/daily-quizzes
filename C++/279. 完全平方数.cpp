//https://leetcode.cn/problems/perfect-squares/

class Solution {
public:
    int numSquares(int n) {
        //dp[i][j]表示：从前i个完全平方数中，挑选，总和正好等于j，的所有选法中，最小的数量
        vector<int> squa;
        for(int i = 1; i * i <= n; i++)
        {
            squa.push_back(i * i);
        }
        int m = squa.size();
        vector<int> dp(n + 1, 0x3f3f3f3f);
        dp[0] = 0;
        for(int i = 1; i <= m; i++)
            for(int j = squa[i - 1]; j <= n; j++)
                    dp[j] = min(dp[j], dp[j - squa[i - 1]] + 1);
        return dp[n];
    }
};