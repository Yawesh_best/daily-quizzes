//https://leetcode.cn/problems/minimum-operations-to-make-binary-array-elements-equal-to-one-i/description/?envType=daily-question&envId=2024-10-18


class Solution {
public:
    int minOperations(vector<int>& nums) {
        int n = nums.size(), ans = 0;
        for(int i = 0; i < n - 2; i++)
        {
            if(nums[i] == 0)
            {
                ans++;
                nums[i + 1] = nums[i + 1] == 0;
                nums[i + 2] = nums[i + 2] == 0;
            }
        }
        if(nums[n - 2] == 0 || nums[n - 1] == 0) return -1;
        return ans;

    }
};