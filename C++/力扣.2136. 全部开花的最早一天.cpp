//https://leetcode.cn/problems/earliest-possible-day-of-full-bloom/description/

class Solution {
public:
    int earliestFullBloom(vector<int>& plantTime, vector<int>& growTime) {
        for (auto it = growTime.cbegin(); int& i : plantTime) {
            i |= *it++ << 16;
        }
        ranges::sort(plantTime);
        return accumulate(plantTime.crbegin(), plantTime.crend(), 0, [now = 0](int grow, int i)mutable {
            now += i & 0xffff;
            return max(grow, now + (i >> 16));
        });
    }
};