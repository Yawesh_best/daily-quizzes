//https://leetcode.cn/problems/find-common-elements-between-two-arrays/description/

class Solution {
public:
    vector<int> findIntersectionValues(vector<int>& nums1, vector<int>& nums2) {
        vector<int> ans(2, 0);
        unordered_set<int> set1(nums1.begin(), nums1.end());
        unordered_set<int> set2(nums2.begin(), nums2.end());
        int count = 0;
        for(int i = nums1.size() - 1; i >=0; i--)
        {
            if(set2.count(nums1[i]) != 0)
            {
                count++;
            }
        }
        ans[0] = count;
        count = 0;
        for(int j = nums2.size() - 1; j >=0; j--)
        {
            if(set1.count(nums2[j]) != 0)
            {
                count++;
            }
        }
        ans[1] = count;
        return ans;
        
    }
};