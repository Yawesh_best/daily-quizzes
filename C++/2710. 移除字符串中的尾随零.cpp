//https://leetcode.cn/problems/remove-trailing-zeros-from-a-string/description/?envType=daily-question&envId=2024-06-29


class Solution {
public:
    string removeTrailingZeros(string num) {
        int n = num.size();
        // 从末尾开始向前找到第一个非零字符
        int i = n - 1;
        while (i >= 1 && num[i] == '0') {
            i--;
        }
        // 返回从开始到非零字符（包括）的所有字符
        return num.substr(0, i + 1);
    }
};