//https://leetcode.cn/problems/pass-the-pillow/description/?envType=daily-question&envId=2023-09-26

class Solution {
public:
    int passThePillow(int n, int time) {
        //if(time == n) return n - 1;
        int t = time % (2 * n - 2);
        if(t >= n) return (2 * n - t) - 1;
        else return t + 1;
    }
};