//https://leetcode.cn/problems/count-tested-devices-after-test-operations/submissions/530658768/?envType=daily-question&envId=2024-05-10


class Solution {
public:
    int countTestedDevices(vector<int>& batteryPercentages) {
        int n = batteryPercentages.size(), ans = 0;
        for(int i = 0; i < n; i++)
        {
            if(batteryPercentages[i])
            {
                ans++;
                for(int j = i + 1; j < n; j++)
                {
                    if(batteryPercentages[j]) --batteryPercentages[j];
                }
            }
        }
        return ans;
    }
};