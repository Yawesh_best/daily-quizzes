//https://leetcode.cn/problems/faulty-keyboard/description/?envType=daily-question&envId=2024-04-01


class Solution {
public:
    string finalString(string s) {
        int n = 0;
        string tmp;
        for(int i = 0; i < s.size(); i++) 
        {
            if(s[i] == 'i') reverse(tmp.begin(), tmp.end());
            else tmp.push_back(s[i]);
        }
        
        return tmp;
    }
};
