//https://leetcode.cn/problems/binary-tree-inorder-traversal/description/


class Solution {
public:
    vector<int> inorderTraversal(TreeNode* root) {
        vector<int> v;
        stack<TreeNode*> tac;
        TreeNode* tmp = root;
        while(tmp || !tac.empty())
        {
            while(tmp)
            {
                
                tac.push(tmp);
                tmp = tmp->left;
            }
            TreeNode* cur = tac.top();
            v.push_back(cur->val);
            tac.pop();
            tmp = cur->right;
        }
        return v;


    }
};