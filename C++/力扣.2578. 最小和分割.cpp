//https://leetcode.cn/problems/split-with-minimum-sum/?envType=daily-question&envId=2023-10-09

class Solution {
public:
    int splitNum(int num) {
        vector<int> nums;
        int n = 0, num1 = 0, num2 = 0;
        while(num)
        {
            nums.push_back(num % 10);
            num /= 10;
            ++n;
        }
        sort(nums.begin(), nums.end());
        for(int i = 0; i < n; i++)
        {
            if(i % 2)
            {
                num1 *= 10;
                num1 += nums[i];
            }
            else
            {
                num2 *= 10;
                num2 += nums[i];
            }
        }
        return num1 + num2;
    }
};