//https://leetcode.cn/problems/count-ways-to-group-overlapping-ranges/?envType=daily-question&envId=2024-03-27

class Solution {
public:
    int countWays(vector<vector<int>>& ranges) {
        int n = ranges.size();
        sort(ranges.begin(), ranges.end(), [](auto& a, auto& b) {
            return a[0] < b[0];
        });
        int ans = 2, r = ranges[0][1], mod = 1e9 + 7;
        for(int i = 1; i < n; i++)
        {
            if(ranges[i][0] > r) ans = ans * 2 % mod;
            r = max(r, ranges[i][1]);
        }
        return ans;
    }
};