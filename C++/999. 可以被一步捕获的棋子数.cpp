//https://leetcode.cn/problems/available-captures-for-rook/?envType=daily-question&envId=2024-12-06

class Solution {
public:
    int numRookCaptures(vector<vector<char>>& board) {
        int ri = 0, rj = 0;
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++) if(board[i][j] == 'R')
            {
                ri = i;
                rj = j;
                break;
            }
        }
        int ans = 0;
        for(int q = ri - 1; q >= 0; q--)
        {
            if(board[q][rj] == 'p') {
                ans++;
                break;
            }
            if(board[q][rj] == 'B') break;
        }
        for(int k = ri + 1; k < 8; k++)
        {
            if(board[k][rj] == 'p') {
                ans++;
                break;
            }

            if(board[k][rj] == 'B') break;
        }
        for(int w = rj - 1; w >= 0; w--)
        {
            if(board[ri][w] == 'p') {
                ans++;
                break;
            }
            if(board[ri][w] == 'B') break;
        }
        for(int e = rj + 1; e < 8; e++)
        {
            if(board[ri][e] == 'p') {
                ans++;
                break;
            }
            if(board[ri][e] == 'B') break;
        }
        return ans;
    }
};