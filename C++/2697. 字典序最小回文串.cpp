//https://leetcode.cn/problems/lexicographically-smallest-palindrome/description/?envType=daily-question&envId=2023-12-13


class Solution {
public:
    string makeSmallestPalindrome(string s) {
        int left = 0, right = s.size() - 1, ans = 0;
        while(left < right)
        {
            if(s[left] != s[right])
            {
                if(s[left] > s[right])
                    s[left] = s[right];
                else
                    s[right] = s[left];
            }
            left++; right--;
        }
        return s;
    }
};