//https://leetcode.cn/problems/detect-capital/description/?envType=daily-question&envId=2024-06-23


class Solution {
public:
    bool detectCapitalUse(string word) {
        int cnt = 0; // 计数大写字母
        for (char c : word)
            cnt += !((c >> 5) & 1);
        return !cnt || word.size() == cnt || (cnt == 1 && (!((word[0] >> 5) & 1)));
    }
};