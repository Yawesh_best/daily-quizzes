//https://leetcode.cn/problems/max-sum-of-a-pair-with-equal-sum-of-digits/description/?envType=daily-question&envId=2023-11-18


class Solution {
public:
    int countnum(int n)
    {
        int ans = 0;
        while(n)
        {
            ans += n % 10;
            n /= 10;
        }
        return ans;
    }
    int maximumSum(vector<int>& nums) {

        int n = nums.size(), ans = -1;
        unordered_map<int, priority_queue<int>> count;
        for(int i = 0; i < n; i++)
        {
            int tmp = countnum(nums[i]);
            count[tmp].push(nums[i]);
        }
        for(auto& n : count)
        {
            if(n.second.size() >= 2)
            {
                int tmp = n.second.top();
                n.second.pop();
                tmp += n.second.top();
                ans = max(ans, tmp);
            }
        }
        return ans;
    }
};