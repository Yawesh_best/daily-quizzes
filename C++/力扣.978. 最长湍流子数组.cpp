//https://leetcode.cn/problems/longest-turbulent-subarray/

class Solution {
public:
    int maxTurbulenceSize(vector<int>& arr) {
        int n = arr.size(), ans = 1;
        vector<int> f(n), g(n);// f上升， g下降
        f[0] = g[0] = 1;
        for(int i = 1; i < n; i++)
        {
            int a = arr[i - 1], b = arr[i];
            if(a > b)
            {
                f[i] = 1;
                g[i] = f[i - 1] + 1;
            }
            else if(a == b)
            {
                f[i] = g[i] = 1;
            }
            else
            {
                f[i] = g[i - 1] + 1;
                g[i] = 1;
            }
            ans = max(ans, max(f[i], g[i]));
        }
        return ans;
    }
};