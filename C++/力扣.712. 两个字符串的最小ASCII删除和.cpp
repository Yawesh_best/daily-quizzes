//https://leetcode.cn/problems/minimum-ascii-delete-sum-for-two-strings/

class Solution {
public:
    int minimumDeleteSum(string s1, string s2) {
        //转化为求最大的公共子序列
        int m = s1.size(), n = s2.size(), ans = 0, sum = 0;
        s1 = ' ' + s1;
        s2 = ' ' + s2;
        vector<vector<int>> dp(m + 1, vector<int>(n + 1));
        for(int j = 1; j <= n; j++) sum += s2[j];
        for(int i = 1; i <= m; i++)
        {
            sum += s1[i];
            for(int j = 1; j <= n; j++)
            {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
                if(s1[i] == s2[j]) dp[i][j] = max(dp[i][j], dp[i - 1][j - 1] + s1[i]);
            }
        }
        return sum - 2 * dp[m][n];
    }
};