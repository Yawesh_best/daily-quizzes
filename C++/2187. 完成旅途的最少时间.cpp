//https://leetcode.cn/problems/minimum-time-to-complete-trips/description/?envType=daily-question&envId=2024-10-05

class Solution {
    typedef long long ll;
public:
    long long minimumTime(vector<int>& time, int totalTrips) {
        ll left = 1, right = (ll)*max_element(time.begin(), time.end()) * (ll)totalTrips, ans = LONG_LONG_MAX;
        while(left <= right){
            ll mid = (right - left) / 2 + left, total = 0;
            for(auto& t : time){
                total += mid / t;
            }
            if(total >= totalTrips){
                ans = min(ans, mid);
                right = mid - 1;
            }
            else{
                left = mid + 1;
            }
        }
        return ans;
    }
};