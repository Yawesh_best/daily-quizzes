//https://leetcode.cn/problems/permutation-difference-between-two-strings/description/?envType=daily-question&envId=2024-08-24

class Solution {
public:
    int findPermutationDifference(string s, string t) {
        unordered_map<char, int> mas, mat;
        int ans = 0, n = s.size();
        for(int i = 0; i < n; i++)
        {
            mas[s[i]] = i;
            mat[t[i]] = i;
        }
        for(int i = 0; i < n; i++)
        {
            ans += abs(mas[s[i]] - mat[s[i]]);
        }
        return ans;
    }
};