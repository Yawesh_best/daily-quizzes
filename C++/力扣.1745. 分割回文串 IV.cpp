//https://leetcode.cn/problems/palindrome-partitioning-iv/description/

class Solution {
public:
    bool checkPartitioning(string s) {
        int n = s.size();
        vector<vector<bool>> dp(n, vector<bool>(n));
        for(int i = n - 1; i >= 0; --i)
            for(int j = i; j < n; j++)
                if(s[i] == s[j])                    
                    dp[i][j] = i + 1 < j ? dp[i + 1][j - 1] : true;
        
        for(int left = 0; left < n; left++)
            for(int right = left + 1; right < n; right++)
                if(dp[0][left] && dp[left + 1][right - 1] && dp[right][n - 1]) return true;
        return false;
    }
};