//https://leetcode.cn/problems/tuple-with-same-product/?envType=daily-question&envId=2023-10-19


class Solution {
public:
    int tupleSameProduct(vector<int>& nums) {
        int n = nums.size(), ans = 0;
        if(n < 4) return 0;
        map<int, int> sub;
        for(int i = 0; i < n - 1; i++)
        {
            for(int j = i + 1; j < n; j++)
            {
                int tmp = nums[i] * nums[j];
                sub[tmp]++;
            }
        }
        for(auto &x : sub)
        { 
            int t = x.second;
            if(t > 1)
                ans += t * (t - 1) * 4;
        }
        return ans;
    }
};