//https://leetcode.cn/problems/find-the-distinct-difference-array/description/?envType=daily-question&envId=2024-01-31https://leetcode.cn/problems/find-the-distinct-difference-array/description/?envType=daily-question&envId=2024-01-31

class Solution {
public:
    vector<int> distinctDifferenceArray(vector<int>& nums) {
        int n = nums.size();
        vector<int> ans(n), suf(n + 1);
        unordered_set<int> s;
        for(int i = n - 1; i;--i)
        {
            s.insert(nums[i]);
            suf[i] = s.size();
        }
        s.clear();
        for(int i = 0; i < n; i++)
        {
            s.insert(nums[i]);
            ans[i] = s.size() - suf[i + 1];
        }
        return ans;
    }
};

