//https://leetcode.cn/problems/removing-minimum-number-of-magic-beans/description/?envType=daily-question&envId=2024-01-18


class Solution {
public:
    long long minimumRemoval(vector<int>& beans) {
        long long total = 0;
        int n = beans.size();    
        sort(beans.begin(), beans.end());
        for(int i = 0; i < n; i++)
            total += beans[i];
        long long ans = total;
        for(int j = 0; j < n; j++)
        {
            ans = min(ans, total - (long long)beans[j] * (n - j));
        }
        return ans;
    }
};

