//https://leetcode.cn/problems/wiggle-subsequence/description/


class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) 
    {
        int n = nums.size(), ans = 1;
        vector<int> f(n, 1), g(n, 1);//f()上升， g()下降
        for(int i = 1; i < n; i++)
        {
            for(int j = 0; j < i; j++)
            {
                if(nums[j] < nums[i])
                {
                    f[i] = max(f[i], g[j] + 1);
                     
                }
                else if(nums[j] > nums[i])
                {
                    g[i] = max(g[i], f[j] + 1);
                }
            }
            ans = max(ans, max(f[i], g[i]));
        }
        return ans;
    }
};