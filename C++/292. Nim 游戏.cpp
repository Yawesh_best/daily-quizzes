//https://leetcode.cn/problems/nim-game/?envType=daily-question&envId=2024-02-04


class Solution {
public:
    bool canWinNim(int n) {
        return n % 4;
    }
};