//https://leetcode.cn/problems/distance-between-bus-stops/?envType=daily-question&envId=2024-09-16https://leetcode.cn/problems/distance-between-bus-stops/?envType=daily-question&envId=2024-09-16

class Solution {
public:
    int distanceBetweenBusStops(vector<int>& distance, int start, int destination) {
        int n = distance.size(), sum = 0, tmp = 0;
        bool indis = false;
        for(int i = 0; i < n; i++)
        {
            if(i == start || i == destination) indis = !indis;
            sum += distance[i];
            if(indis) tmp += distance[i];
        }
        return min(sum - tmp, tmp);
    }
};