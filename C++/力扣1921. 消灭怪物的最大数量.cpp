//https://leetcode.cn/problems/eliminate-maximum-number-of-monsters/?envType=daily-question&envId=2023-09-03

class Solution {
public:
    int eliminateMaximum(vector<int>& dist, vector<int>& speed) {
        int n = dist.size();
        vector<int> time(n);
        for(int i = 0; i < n; i++)
        {
            time[i] = (dist[i] - 1) / speed[i] + 1;
            //(dist[i] - 1) 是为了确保在计算时间时，
            //即使 dist[i] 是整除 speed[i] 的情况下，
            //也能得到正确的结果。这是因为 (dist[i] - 1) / speed[i] 的结果会向下取整，
            //而加上 1 则可以将结果向上取整，确保计算出的时间不会小于实际需要的时间。
        }
        sort(time.begin(), time.end());

        for(int i = 0; i < n; i++)
        {
            if(time[i] <= i)
            return i;
        }
        return n;
    }
};