//https://leetcode.cn/problems/maximum-enemy-forts-that-can-be-captured/description/?envType=daily-question&envId=2023-09-02

class Solution {
public:
    int captureForts(vector<int>& forts) {
        int ans = 0, xray = -1;
        for(int i = 0; i < forts.size(); i++)
        {
            if(forts[i] == -1 || forts[i] == 1)
            {
                if(xray>=0 && forts[i] != forts[xray])
                {
                    ans = max(ans, i - xray - 1);
                }
                xray = i;
            }
        }
        return ans;
    }
};