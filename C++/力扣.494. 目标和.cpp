//https://leetcode.cn/problems/target-sum/

class Solution {
public:
    int findTargetSumWays(vector<int>& nums, int target) {
        int sum = 0;
        for(auto e : nums)
        {
            sum += e;
        }
        int aim = (sum + target) / 2;
        if(aim < 0 || (sum + target) % 2) return 0;
        vector<int> dp(aim + 1);
        dp[0] = 1;
        for(int i = 1; i <= nums.size(); i++)
        {
            for(int j = aim; j >= nums[i - 1]; j--)
            {
                dp[j] += dp[j - nums[i - 1]];
            }
        }
        return dp[aim];

    }
};