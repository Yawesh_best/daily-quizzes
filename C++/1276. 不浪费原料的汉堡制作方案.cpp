//https://leetcode.cn/problems/number-of-burgers-with-no-waste-of-ingredients/?envType=daily-question&envId=2023-12-25



class Solution {
public:
    vector<int> numOfBurgers(int tomatoSlices, int cheeseSlices) {
        if(tomatoSlices % 2 || tomatoSlices < 2 * cheeseSlices || tomatoSlices > 4 * cheeseSlices) return {};
        int tnum = 0, cnum = cheeseSlices;
        tomatoSlices -= cheeseSlices*2;
        while(tomatoSlices)
        {
            tomatoSlices -= 2;
            tnum++;
            cnum--;
        }
        return{tnum, cnum};
    }
};