//https://leetcode.cn/problems/minimum-deletions-to-make-array-beautiful/submissions/483639832/?envType=daily-question&envId=2023-11-21


class Solution {
public:
    int minDeletion(vector<int>& nums) {
        int n = nums.size();
        if(n == 0) return 0;
        int flag = -1, ans = 0;
        for(int i = 0; i < n; i++)
        {
            if(flag == -1) flag = nums[i];
            if(flag != nums[i])
            {
                flag = -1;
                ans += 2;
            }
        }
        return n - ans;
    }
};