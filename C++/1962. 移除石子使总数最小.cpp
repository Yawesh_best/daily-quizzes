//https://leetcode.cn/problems/remove-stones-to-minimize-the-total/?envType=daily-question&envId=2023-12-23


class Solution {
public:
    int minStoneSum(vector<int>& piles, int k) {
        priority_queue<int> pq;
        int ans = 0;
        for(int i = 0; i < piles.size(); i++)
        {
            pq.push(piles[i]);
            ans += piles[i];
        }
        for(int i = 0; i < k; i++)
        {
            int tmp = pq.top();
            pq.pop();
            ans -= tmp / 2;
            pq.push((tmp + 1) / 2);
        }
        return ans;
    }
};