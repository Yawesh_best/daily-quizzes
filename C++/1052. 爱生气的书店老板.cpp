//https://leetcode.cn/problems/grumpy-bookstore-owner/description/?envType=daily-question&envId=2024-04-23


class Solution {
public:
    int maxSatisfied(vector<int>& customers, vector<int>& grumpy, int minutes) {
        int n = customers.size();
        int total = 0;
        vector<int> sum(n + 1);
        for (int i = 1; i <= n; i++) {
            sum[i] = sum[i - 1];
            if (grumpy[i - 1] == 1) {
                sum[i] += customers[i - 1];
            } else {
                total += customers[i - 1];
            }
        }

        int maxval = INT_MIN;
        for (int i = minutes; i <= n; i++) {
            maxval = max(maxval, sum[i] - sum[i - minutes]);
        }

        return total + maxval;
    }
};

