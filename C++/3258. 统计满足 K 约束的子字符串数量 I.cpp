//https://leetcode.cn/problems/count-substrings-that-satisfy-k-constraint-i/description/?envType=daily-question&envId=2024-11-12

class Solution {
public:
    int countKConstraintSubstrings(string s, int k) {
        int n = s.size(), ans = 0;
        for(int i = 0; i < n; i++)
        {
            int num0 = 0;
            int num1 = 0;
            for(int j = i; j < n; j++)
            {
                if(s[j] == '0') num0++;
                if(s[j] == '1') num1++;
                if(num0 > k && num1 > k) break;
                ans++;
            }
        }
        return ans;
    }
};