//https://www.nowcoder.com/practice/22948c2cad484e0291350abad86136c3?tpId=37&&tqId=21331&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking


#include <iostream>
using namespace std;

int main() {
    int a, b;
    cin>>a>>b;
    int ma = max(a, b), ans = ma;
    while(ans < a * b)
    {
        if((ans % a == ans % b) && ans % a == 0)
        {
            break;
        }
        else 
        {
            ans++;
        }
    }
    cout<< ans;

}
