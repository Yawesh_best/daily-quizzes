//https://leetcode.cn/problems/arithmetic-slices-ii-subsequence/

class Solution {
public:
    int numberOfArithmeticSlices(vector<int>& nums) {
        int n = nums.size(), ans = 0;
        vector<vector<int>> count(n, vector<int>(n));
        for(int j = 2; j < n; j++)
        {
            for(int i = 1; i < j; i++)
            {
                long long a = 2 * (long long)nums[i] - nums[j];
                for(int k = 0; k < i; k++)
                {
                    if(nums[k] == a)
                        count[i][j] += count[k][i] + 1;
                }
                ans += count[i][j];
            }
        }
        return ans;
    }
};