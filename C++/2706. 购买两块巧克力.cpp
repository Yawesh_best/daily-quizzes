//https://leetcode.cn/problems/buy-two-chocolates/description/?envType=daily-question&envId=2023-12-29





class Solution {
public:
    int buyChoco(vector<int>& prices, int money) {
        int tmp = money;
        sort(prices.begin(), prices.end());
        money -= (prices[0] + prices[1]);

        return money < 0 ? tmp : money;
    }
};