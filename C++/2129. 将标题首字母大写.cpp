//https://leetcode.cn/problems/capitalize-the-title/submissions/510364362/?envType=daily-question&envId=2024-03-11


class Solution {
public:
    string capitalizeTitle(string title) {  
        int n = title.size();
        for(int i = 0;i<n;i++)
        {
            if(title[i]>='A'&&title[i]<='Z') title[i] = title[i]+32;
        }
        int first = 0;
        int pos = title.find(' ');
        while(pos!=string::npos)
        {
            if(pos-first>2) title[first] = title[first]-32;
            first = pos+1;
            pos = title.find(' ',first);
        }
        if(n-first>2) title[first] = title[first]-32;
        return title;
    }
};