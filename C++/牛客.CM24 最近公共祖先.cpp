//https://www.nowcoder.com/practice/70e00e490b454006976c1fdf47f155d9?tpId=8&&tqId=11017&rp=1&ru=/activity/oj&qru=/ta/cracking-the-coding-interview/question-ranking


class LCA {
public:
    int getLCA(int a, int b) {
        if(a == b) return a;
        else if (a < b) return getLCA(a, b / 2);
        else return getLCA(a / 2,b);
    }
};