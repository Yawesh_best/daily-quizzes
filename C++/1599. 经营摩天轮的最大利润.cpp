//https://leetcode.cn/problems/maximum-profit-of-operating-a-centennial-wheel/?envType=daily-question&envId=2024-01-01https://leetcode.cn/problems/maximum-profit-of-operating-a-centennial-wheel/?envType=daily-question&envId=2024-01-01

class Solution {
public:
    int minOperationsMaxProfit(vector<int>& customers, int boardingCost, int runningCost) {
        int operation = 0, waiting = 0, totalProfit = 0, res = -1, maxProfit = 0;
        for(const auto& num: customers) {
            // 顾客到达
            waiting += num;
            // 顾客进入摩天轮
            totalProfit += min(waiting, 4) * boardingCost - runningCost;
            waiting -= min(waiting, 4);
            ++operation;
            // 记录最大利润
            if(totalProfit > maxProfit) {
                maxProfit = totalProfit;
                res = operation;
            }
        }
        // 还有顾客在等待
        while(waiting > 0) {
            // 顾客进入摩天轮
            totalProfit += min(waiting, 4) * boardingCost - runningCost;
            waiting -= min(waiting, 4);
            ++operation;
            // 记录最大利润
            if(totalProfit > maxProfit) {
                maxProfit = totalProfit;
                res = operation;
            }
        }
        return res;
    }
};