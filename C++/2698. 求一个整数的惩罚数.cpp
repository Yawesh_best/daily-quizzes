//https://leetcode.cn/problems/find-the-punishment-number-of-an-integer/?envType=daily-question&envId=2023-10-25

class Solution {
public:
    int punishmentNumber(int n) {
        vector<int> ans = {1,9,10,36,45,55,82,91,99,100,
        235,297,369,370,379,414,657,675,703,756,792,909,
        918,945,964,990,991,999,1000};
        int sum = 0;
        for(int i = 1; i <= n; i++)
        {
            for(int j = 0; j < ans.size(); j++)
            {
                if(i == ans[j]) sum += i * i;
            }
        }
        return sum;
    }
};