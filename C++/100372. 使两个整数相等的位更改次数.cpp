//https://leetcode.cn/problems/number-of-bit-changes-to-make-two-integers-equal/description/

class Solution {
public:
    int minChanges(int n, int k) {
        int ans = 0;
        
        while(n)
        {
            int nbit = n % 2;
            int kbit = k % 2;
            n /= 2;
            k /= 2;
            if(nbit != kbit && kbit == 0)
            {
                ans++;
            }
            else if(nbit != kbit && kbit)
            {
                return -1;
            }
            else
            {
                continue;
            }
        }
        if(k >0) ans = -1;
        return ans;
    }
};