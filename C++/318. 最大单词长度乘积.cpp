//https://leetcode.cn/problems/maximum-product-of-word-lengths/description/?envType=daily-question&envId=2023-11-06v




class Solution {
public:
    int maxProduct(vector<string>& words) {
        vector<int> bit(words.size(), 0);
        int res = 0;
        for(int i = 0; i < words.size(); i++) 
            for(int j = 0; j < words[i].length(); j++) 
                bit[i] |= 1 << (words[i][j] - 'a');
        for(int i = 0; i < words.size() - 1; i++) 
            for(int j = i + 1; j < words.size(); j++) 
                if((bit[i] & bit[j]) == 0)
                    res = max(res, static_cast<int>(words[i].length() * words[j].length()));
        return res;
    }
};