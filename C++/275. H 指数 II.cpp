//https://leetcode.cn/problems/h-index-ii/?envType=daily-question&envId=2023-10-30



class Solution {
public:
    int hIndex(vector<int>& citations) {
        int h = 0, i = citations.size() - 1;
        while (i >= 0 && citations[i] > h) {
            h++;
            i--;
        }
        return h;
    }
};