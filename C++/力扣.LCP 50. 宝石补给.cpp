//https://leetcode.cn/problems/WHnhjV/description/?envType=daily-question&envId=2023-09-15

class Solution {
public:
    int giveGem(vector<int>& gem, vector<vector<int>>& operations) {
        int ma = 0, mi = 10000;
        for(int i = 0; i < operations.size(); i++)
        {
            int tmp = gem[operations[i][0]] / 2;
            gem[operations[i][0]] -= tmp;
            gem[operations[i][1]] += tmp;
        }
        for(int i = 0; i < gem.size(); i++)
        {
            ma = max(ma, gem[i]);
            mi = min(mi, gem[i]);
        }
        return ma - mi;
    }
};