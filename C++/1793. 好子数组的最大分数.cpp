//https://leetcode.cn/problems/maximum-score-of-a-good-subarray/?envType=daily-question&envId=2024-03-19



class Solution {
public:
    int maximumScore(vector<int>& nums, int k) {
        int left = k, right = k, ans = nums[k], minVal = nums[k];
        while (left > 0 || right < nums.size() - 1) 
        {
            if (left > 0 && (right == nums.size() - 1 || nums[left - 1] > nums[right + 1])) 
            {
                left--;
                minVal = min(minVal, nums[left]);
            } 
            else 
            {
                right++;
                minVal = min(minVal, nums[right]);
            }
            ans = max(ans, minVal * (right - left + 1));
        }
        return ans;
    }
};
