//https://leetcode.cn/problems/find-pivot-index/?envType=daily-question&envId=2024-07-08


class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        for(int i{};i<nums.size();++i){
            if(accumulate(nums.begin(),nums.begin()+i,0) == accumulate(nums.begin()+i+1,nums.end(),0)) return i;
        }
        return -1;
    }
};