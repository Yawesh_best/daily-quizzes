//https://leetcode.cn/problems/length-of-longest-subarray-with-at-most-k-frequency/description/

#include <vector>
#include <unordered_map>

class Solution {
public:
    int maxSubarrayLength(vector<int>& nums, int k) {
        int n = nums.size();
        unordered_map<int, int> cnt;
        int l = 0, r = 0, ans = 0;
        while(r < n) {
            ++cnt[nums[r]];
            while(l <= r && cnt[nums[r]] > k) {
                --cnt[nums[l++]];
            }
            ans = max(ans, r - l + 1);
            ++r;
        }
        return ans;
    }
};