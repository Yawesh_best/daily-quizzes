//https://leetcode.cn/problems/unique-substrings-in-wraparound-string/description/


class Solution {
public:
    int findSubstringInWraproundString(string s) {
        int n = s.size(), ans = 0;
        vector<int> dp (n), num (26);
        dp[0] = 1;
        num[s[0] - 'a']++;
        for(int i = 1; i < n; i++)
        {
            if((s[i] == s[i - 1] + 1) || (s[i - 1] == 'z' && s[i] == 'a'))
            {
                dp[i] = dp[i - 1] + 1;
            }
            else
            {
                dp[i] = 1;
            }
            num[s[i] - 'a'] = max(num[s[i] - 'a'], dp[i]);
        }
        for(int i = 0; i < num.size(); i++)
        {
            ans += num[i];
        }
        return ans;
    }
};