//https://leetcode.cn/problems/minimum-rectangles-to-cover-points/?envType=daily-question&envId=2024-07-31


class Solution {
public:
    int minRectanglesToCoverPoints(vector<vector<int>>& points, int w) {
        sort(points.begin(), points.end(), [] (const vector<int>& v1, const vector<int>& v2)->bool {
            return v1[0] < v2[0];
        });
        int n = points.size(), ans = 1, cur = points[0][0] + w;
        for(int i = 1; i < n; i++)
        {
            if(cur < points[i][0]) 
            {
                cur = points[i][0] + w;
                ans++;
            }
            
        }
    return ans;
    }
};