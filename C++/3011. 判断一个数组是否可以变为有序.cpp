//https://leetcode.cn/problems/find-if-array-can-be-sorted/submissions/546343407/?envType=daily-question&envId=2024-07-13


class Solution {
public:
    bool canSortArray(vector<int>& nums) {
        int preMax=-1;
        int max1=nums[0];
        for(int i=1;i<nums.size();i++)
        {
            if(__builtin_popcount(nums[i]) ==__builtin_popcount(nums[i-1]))
            {
                //更新当前组最大值
                max1= max(max1,nums[i]);
            }
            else
            { 
                //换组了，把之前组的最大值赋值给premax
                preMax = max1;
                //下一组最大值等于当前组的第一个元素
                max1=nums[i];
            }
            //判断当前元素是否小于上一组的最大值，无法组成有效排序
            if(nums[i]<preMax)
                    return false;
        }
        
        return true;
    }

};