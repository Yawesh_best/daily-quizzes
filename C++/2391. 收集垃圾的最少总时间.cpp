//https://leetcode.cn/problems/minimum-amount-of-time-to-collect-garbage/description/?envType=daily-question&envId=2024-05-11


#include <ranges>
class Solution {
public:
    int garbageCollection(vector<string>& garbage, vector<int>& travel) {
        int ans = ranges::count_if(garbage | views::join, identity{})
                + reduce(travel.begin(), travel.end(), 0) * 3;
        for (char ch : "MPG"s) {
            for (int i : views::iota(1uz, garbage.size()) | views::reverse | views::take_while([&](int i) { return garbage[i].find(ch) == string::npos; })) {
            //for (int i = travel.size(); i && garbage[i].find(ch) == string::npos; i--) {
                ans -= travel[i - 1];
            }
        }
        return ans;
    }
};