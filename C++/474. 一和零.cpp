//https://leetcode.cn/problems/ones-and-zeroes/

class Solution {
public:
    int findMaxForm(vector<string>& strs, int m, int n) {
        //dp[i][j][k]表⽰：从前 i 个字符串中挑选，字符 0 的个数不超过 j ，字符 1 的个数不超过 k ，所有的选法中，最⼤的⻓度
        vector<vector<int>> dp(m + 1 ,vector<int>(n + 1));

        for(int i = 1; i <= strs.size(); i++)
        {
            int a = 0, b = 0;
            for(auto str : strs[i - 1])
            {
                if(str == '0') ++a;
                else ++b;
            }
            for(int j = m; j >= a; j--)
                for(int k = n; k >= b; k--)
                        dp[j][k] = max(dp[j][k], dp[j - a][k - b] + 1);
        }
        return dp[m][n];
    }
};