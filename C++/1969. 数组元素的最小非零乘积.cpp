//https://leetcode.cn/problems/minimum-non-zero-product-of-the-array-elements/description/?envType=daily-question&envId=2024-03-20



class Solution {
public:
    const int mod = 1000000007;
    long long  quick(long long num, long long n)
    {
        long long ans = 1;
        num %= mod;
        //快速幂迭代写法
        while(n)
        {
            if(n%2==1) ans =  ans* num % mod;
            num = (num* num)%mod;
            n /=2;
        }
        return ans;

    }
    int minNonZeroProduct(int p) {
        if(p ==1) return 1;
    // 这里不能直接再pow上加减因为 pow前面的数太大 后面的1直接不算了
        long long num = pow(2, p);  // long long num = pow(2, p) -1;
        num--;
        long long ans = quick(num-1, num/2)%mod;
        num = num %mod;
        return (num* ans) %mod;
    }
};