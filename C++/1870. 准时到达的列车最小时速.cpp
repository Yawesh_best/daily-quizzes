//https://leetcode.cn/problems/minimum-speed-to-arrive-on-time/description/?envType=daily-question&envId=2024-10-02https://leetcode.cn/problems/minimum-speed-to-arrive-on-time/description/?envType=daily-question&envId=2024-10-02

class Solution {
public:
    int minSpeedOnTime(vector<int>& dist, double hour) {
        int l = 0, r = 10000000, n = dist.size(), res = -1;
        while (l <= r)
        {
            int mid = (l + r) / 2;
            if (mid == 0)
            {
                l = mid + 1;
                continue;
            }
            int t0 = 0;
            for (int i = 0; i < n - 1; i++)
            {
                t0 += ((dist[i] + (mid - 1)) / mid);
            }
            double t = t0 + dist.back() * 1.0 / mid;
            if (t > hour + 1e-9)
            {
                l = mid + 1;
            }
            else
            {
                res = mid;
                r = mid - 1;
            }
        }
        return res;
    }
};