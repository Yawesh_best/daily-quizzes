//https://leetcode.cn/problems/minimum-rounds-to-complete-all-tasks/description/?envType=daily-question&envId=2024-05-14


class Solution {
public:
    int minimumRounds(vector<int>& tasks) {
        unordered_map<int, int> umap;
        int n = tasks.size();
        for(int i=0; i<n; i++){
            umap[tasks[i]]++;
        }
        int ans = 0;
        for(auto & m: umap){
            if(m.second==1) return -1;
            ans+=(m.second+2)/3;
        }
        return ans;
    }
};