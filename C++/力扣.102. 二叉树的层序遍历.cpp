//https://leetcode.cn/problems/binary-tree-level-order-traversal/description/


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:

    vector<vector<int>> levelOrder(TreeNode* root) {
        vector<vector<int>> ans;
        dfs(ans, root, 0);
        return ans;
    }
    void dfs(vector<vector<int>>& ans, TreeNode* root, int level)
    {
        if(root == nullptr) return;
        if(level >= ans.size())
        {
            ans.push_back(vector<int>());
        }
        ans[level].push_back(root->val);
        dfs(ans, root->left, level + 1);
        dfs(ans, root->right, level + 1);
    }
};


//非递归版本

    vector<vector<int>> levelOrder(TreeNode* root) {

        queue<TreeNode*> q;
        int levelsize = 0;
        if(root)
        {
            q.push(root);
            levelsize = 1;
        }
        vector<vector<int>> vv;
        
        while(!q.empty())
        {
            vector<int> v;
            while(levelsize--)
            {
                TreeNode* front = q.front();
                q.pop();
                
                v.push_back(front->val);

                if(front->left)
                    q.push(front->left);
                if(front->right)
                    q.push(front->right);
            }
            vv.push_back(v);
            levelsize = q.size();
        }
    return vv;
    }