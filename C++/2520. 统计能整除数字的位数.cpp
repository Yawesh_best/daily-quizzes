//https://leetcode.cn/problems/count-the-digits-that-divide-a-number/?envType=daily-question&envId=2023-10-26


class Solution {
public:
    int countDigits(int num) {
        int ans = 0, tmp = num;
        while(tmp)
        {
            if(num % (tmp % 10) == 0) ans++;
            tmp /= 10;
        }
        return ans;
    }
};