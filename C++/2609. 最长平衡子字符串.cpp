//https://leetcode.cn/problems/find-the-longest-balanced-substring-of-a-binary-string/?envType=daily-question&envId=2023-11-08



class Solution {
public:
    int findTheLongestBalancedSubstring(string s) {
        int n = s.size(), ans = 0, count0 = 0, count1 = 0;
        for(int i = 0; i < n; i++)
        {
            if(s[i] == '0')
            {
                if(count1 == 0) 
                    count0++;
                else
                {
                    count1 = 0;
                    count0 = 1;
                }
            }
            else
            {
                if(count1 < count0)
                {
                    count1++;
                    ans = max(count1 * 2, ans);
                }
            }
        }
        return ans;
    }
};