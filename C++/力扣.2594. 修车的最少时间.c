//https://leetcode.cn/problems/minimum-time-to-repair-cars/submissions/?envType=daily-question&envId=2023-09-07

class Solution {
public:
    using ll = long long;
    long long repairCars(vector<int>& ranks, int cars) 
    {
        ll left = 1, right = 1ll * ranks[0] * cars * cars;
        auto check = [&](ll m) 
        {
            ll cnt = 0;
            for (auto x : ranks) 
            {
                cnt += sqrt(m / x);
            }
            return cnt >= cars;
        };
        while (left < right) 
        {
            ll m = left + right >> 1;
            if (check(m)) 
            {
                right = m;
            } else 
            {
                left = m + 1;
            }
        }
        return left;
    }
};
