//https://leetcode.cn/problems/longest-alternating-subarray/description/?envType=daily-question&envId=2024-01-23


class Solution {
public:
    int alternatingSubarray(vector<int>& nums) {
        int n = nums.size();
        int max_l{}, l = 1;
        int t = nums[0], aux = 1, ts = 1;
        for(int i = 1; i < n; i++){
            if(nums[i] == t + (aux % 2)){
                ts = 1;
                l++;
                aux++;
                max_l = max(max_l,l);
            }
            else{
                if(--ts >= 0) i--;
                aux = 1;
                l = 1;
                t = nums[i];
            }
        }
        if(!max_l) return -1;
        return max_l;
    }
};