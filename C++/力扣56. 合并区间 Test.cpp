//链接：https://leetcode.cn/problems/merge-intervals/

class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) 
    {
        vector<vector<int>> ans;
        int n = intervals.size();
        if (n <= 1) 
        {
            return intervals;
        }
        // 对区间数组按照起始位置进行排序
        sort(intervals.begin(), intervals.end());
        // 合并区间
        for (int i = 0; i < n; i++) 
        {
            // 当前区间的起始位置和结束位置
            int start = intervals[i][0];
            int end = intervals[i][1];
            // 查看结果数组中的最后一个区间是否可以和当前区间合并
            if (!ans.empty() && ans.back()[1] >= start) 
            {
                // 更新最后一个区间的结束位置
                ans.back()[1] = max(ans.back()[1], end);
            } 
            else 
            {
                // 将当前区间添加到结果数组中
                ans.push_back({start, end});
            }
        }
        return ans;
    }
};