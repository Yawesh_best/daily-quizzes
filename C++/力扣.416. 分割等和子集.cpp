//https://leetcode.cn/problems/partition-equal-subset-sum/


class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int n = nums.size(), sum = 0;
        for(auto x : nums) sum += x;
        if(sum % 2) return false; // 如果不能平分，直接返回 false
        int aim = sum / 2; // 定义⼀下⽬标值
        // 建表表⽰在前 i 个元素中选择，所有的选法中，能否凑成总和为 j 这个数
        vector<bool> dp(aim + 1); 
        dp[0] = true;
        for(int i = 1; i <= n; i++)
        {
            for(int j = aim; j >= nums[i - 1]; j--)
            {
                dp[j] = dp[j] || dp[j - nums[i - 1]];
            }
        }
        return dp[aim];
    }
};