//https://leetcode.cn/problems/maximum-sum-circular-subarray/description/

class Solution {
public:
    int maxSubarraySumCircular(vector<int>& nums) {
        int n = nums.size(), sum = 0, mi = nums[0], ma = nums[0];
        vector<vector<int>> dp (2, vector<int> (n + 1));

        for(int i = 1; i <= n; i++)
        {
            dp[0][i] = max(nums[i - 1], nums[i - 1] + dp[0][i - 1]);
            dp[1][i] = min(nums[i - 1], nums[i - 1] + dp[1][i - 1]);
            sum += nums[i - 1];
            ma = max(ma, dp[0][i]);
            mi = min(mi, dp[1][i]);
        }
        return sum == mi ? ma : max(ma, sum - mi);

    }
};