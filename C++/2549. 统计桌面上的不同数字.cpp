//https://leetcode.cn/problems/count-distinct-numbers-on-board/description/?envType=daily-question&envId=2024-03-23

class Solution {
public:
    int distinctIntegers(int n) {
        return n > 1 ? n - 1 : 1;
    }
};