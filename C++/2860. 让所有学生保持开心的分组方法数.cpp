// https://leetcode.cn/problems/happy-students/?envType=daily-question&envId=2024-09-04

class Solution {
public:
    int countWays(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int res = 1 + (nums.front() > 0);
        for (auto i = nums.begin(), j = i; j != nums.end(); ++j) {
            if (*i != *j) {
                res += (j - nums.begin() < *j && j - nums.begin() > *(j - 1));
                i = j;
            }
        }
        return res;
    }
};