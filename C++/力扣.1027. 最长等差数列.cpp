//https://leetcode.cn/problems/longest-arithmetic-subsequence/description/


class Solution {
public:
    int longestArithSeqLength(vector<int>& nums) {
        int n = nums.size();
        int ans = 2;
        vector<vector<int>> dp(n, vector<int>(n, 2));
        unordered_map<int, int> hash;
        hash[nums[0]] = 0;
        //for(int i = 0; i < n; i++) 
        //  hash[nums[i]] = i;
        for(int i = 1; i < n; i++)
        {
            for(int j = i + 1; j < n; j++)
            {
                int a = 2 * nums[i] - nums[j];
                if(hash.count(a))
                {
                    dp[i][j] = dp[hash[a]][i] + 1;
                }
                ans = max(ans, dp[i][j]);
            }
            hash[nums[i]] = i;
        }
        return ans;
    }
};
