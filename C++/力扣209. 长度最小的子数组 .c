//https://leetcode.cn/problems/minimum-size-subarray-sum/description/

/*class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int n = nums.size();
        if(nums[n - 1] >= target) return 1;
        int right = n - 1, left = right - 1;
        while(left >= 0)
        {
            int sum = nums[right];
            while(left >= 0 && (nums[left + 1] == nums[left] + 1 || nums[left + 1] == nums[left]))
            {
                sum += nums[left];
                if(sum >= target) return right - left + 1;
                else
                {
                    --left;
                }
            }
            --right;
            left = right - 1;
        }
        return 0;
    }
};*/

class Solution {
public:
    int minSubArrayLen(int s, vector<int>& nums) {
        int n = nums.size();
        if (n == 0) {
            return 0;
        }
        int ans = INT_MAX;
        vector<int> sums(n + 1, 0); 
        // 为了方便计算，令 size = n + 1 
        // sums[0] = 0 意味着前 0 个元素的前缀和为 0
        // sums[1] = A[0] 前 1 个元素的前缀和为 A[0]
        // 以此类推
        for (int i = 1; i <= n; i++) {
            sums[i] = sums[i - 1] + nums[i - 1];
        }
        for (int i = 1; i <= n; i++) {
            int target = s + sums[i - 1];
            auto bound = lower_bound(sums.begin(), sums.end(), target);
            if (bound != sums.end()) {
                ans = min(ans, static_cast<int>((bound - sums.begin()) - (i - 1)));
            }
        }
        return ans == INT_MAX ? 0 : ans;
    }
};