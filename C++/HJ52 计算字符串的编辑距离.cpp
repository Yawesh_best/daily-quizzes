//https://www.nowcoder.com/practice/3959837097c7413a961a135d7104c314?tpId=37&&tqId=21275&rp=1&ru=/activity/oj&qru=/ta/huawei/question-ranking

//#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main() 
{
    string str1, str2;
    while (cin >> str1 >> str2) 
    {
        int m = str1.size(), n = str2.size();
        vector<vector<int>> dp(m + 1, vector<int> (n + 1));
        for(int i = 0; i <= m; ++i)
            dp[i][0] = i;
        for(int j = 0; j <= n; ++j)
            dp[0][j] = j;
        //F(i,j) = min { F(i-1,j）+1, F(i,j-1) +1, F(i-1,j-1) + (s1[i]==s2[j]?0:1) }
        for(int i = 1; i <= m; i++)
        {
            for(int j = 1; j <= n; j++)
            {
                if(str1[i - 1] == str2[j - 1])
                    dp[i][j] = min(dp[i - 1][j - 1], min(dp[i][j - 1] + 1, dp[i - 1][j] + 1));
                else
                    dp[i][j] = min(min(dp[i][j - 1] + 1, dp[i - 1][j] + 1), dp[i - 1][j - 1] + 1);
            }
        }
        cout<< dp[m][n]<<endl;
    }
}

