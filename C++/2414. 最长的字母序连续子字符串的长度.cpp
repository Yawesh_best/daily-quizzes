//https://leetcode.cn/problems/length-of-the-longest-alphabetical-continuous-substring/?envType=daily-question&envId=2024-09-19

class Solution {
public:
    int longestContinuousSubstring(string s) {
        int ans = 0, n = s.size();
        for(int i = 0; i < n; i++)
        {
            int j = i + 1;
            char tmp = s[i];
            while(j < n)
            {
                if(s[j] == tmp + 1) tmp = s[j++];
                else break;
            }
            ans = max(ans, j - i);
            if(ans == 26) break;
        }
        return ans;
    }
};