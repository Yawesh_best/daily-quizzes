//https://leetcode.cn/problems/maximum-strength-of-a-group/?envType=daily-question&envId=2024-09-03

class Solution {
public:
    long long maxStrength(vector<int>& nums) {
        long long ans = 1, count = 0, mi = INT_MAX, zer = 0;
        for(int i = 0; i < nums.size(); i++)
        {
            if(nums[i] == 0) zer++;
            else if(nums[i] > 0) ans *= nums[i];
            else
            {
                count++;
                if(abs(nums[i]) < mi)
                    mi = abs(nums[i]);
                ans *= abs(nums[i]);
            }
        }
        if(count % 2 == 1) ans /= mi;
        if(nums.size() == 1) ans = nums[0];
        else if(zer == nums.size() || (zer == nums.size() - 1 && count == 1)) return 0;
        
        return ans;
    }
};