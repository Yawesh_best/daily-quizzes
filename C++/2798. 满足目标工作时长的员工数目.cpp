//https://leetcode.cn/problems/number-of-employees-who-met-the-target/description/?envType=daily-question&envId=2024-04-30https://leetcode.cn/problems/number-of-employees-who-met-the-target/description/?envType=daily-question&envId=2024-04-30
class Solution {
public:
    int numberOfEmployeesWhoMetTarget(vector<int>& hours, int target) {
        int ans = 0;
        for(auto n : hours) if(n >= target) ans++;
        return ans;
    }
};