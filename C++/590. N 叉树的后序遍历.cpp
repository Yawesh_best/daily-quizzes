//https://leetcode.cn/problems/n-ary-tree-postorder-traversal/submissions/502939407/?envType=daily-question&envId=Invalid%20Date



class Solution {
private:
    vector<int> res;
    void postorder_helper(Node* root) {
        if(root == nullptr) return;
        for(const auto& child: root->children) {
            postorder_helper(child);
        }
        res.push_back(root->val);
    }
public:
    vector<int> postorder(Node* root) {
        postorder_helper(root);
        return res;
    }
};