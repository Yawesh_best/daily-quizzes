//https://leetcode.cn/problems/movement-of-robots/?envType=daily-question&envId=2023-10-10

class Solution {
public:
    int sumDistance(vector<int>& nums, string s, int d) {
        int mol = 1e9 + 7;
        int n = nums.size();
        vector<long long> a(n);
        for(int i = 0; i < n; i++)
        {
            if(s[i] == 'R') a[i] = (long long) nums[i] + d;
            else a[i] = (long long) nums[i] - d;
        }
        sort(a.begin(), a.end());
        long long ans = 0, sum = 0;
        for (int i = 0; i < n; i++) 
        {
            ans = (ans + i * a[i] - sum) % mol;
            sum += a[i];
        }
        return ans;
    }
};