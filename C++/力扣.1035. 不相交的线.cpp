//https://leetcode.cn/problems/uncrossed-lines/


class Solution {
public:
    int maxUncrossedLines(vector<int>& nums1, vector<int>& nums2) {
        int lon1 = nums1.size(), lon2 = nums2.size(), ans = 0;
        vector<vector<int>> dp (lon1 + 1, vector<int>(lon2 + 1));
        
        for(int i = 1; i <= lon1; i++)
        {
            for(int j = 1; j<= lon2; j++)
            {
                if(nums1[i - 1] == nums2[j - 1])
                {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                }
                else
                {
                    dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
                }
                ans = max(ans, dp[i][j]);
            }
        }
        return ans;
    }
};