//https://leetcode.cn/problems/find-all-numbers-disappeared-in-an-array/description/

class Solution {
public:
    vector<int> findDisappearedNumbers(vector<int>& nums) {
        vector<int> map(nums.size()+2);
        vector<int> ans;
        for(int i = 0; i < nums.size(); i++)
        {
            ++map[nums[i] ];
        }
        for(int i = 1; i <= nums.size(); i++)
        {
            if(map[i] == 0)
            {
                ans.push_back(i);
            }
        }
        return ans;
    }
};