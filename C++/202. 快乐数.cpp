//https://leetcode.cn/problems/happy-number/description/



class Solution {
public:
    int count(int n)
    {
        int ans = 0;
        while(n)
        {
            ans += ((n % 10) * (n % 10));
            n /= 10;
        }
        return ans;
    }
    bool isHappy(int n) {
        int fast = count(n), slow = n;
        while(fast != slow)
        {
            fast = count(count(fast));
            slow = count(slow);
        }
        return slow == 1;
    }
};