//https://leetcode.cn/problems/count-unique-characters-of-all-substrings-of-a-given-string/?envType=daily-question&envId=2023-11-26


class Solution {
public:
    int uniqueLetterString(string s) {
        int n = s.size();
        vector<int> p(26,-1);
        vector<int> l(n,0);
        vector<int> r(n,0);
        for(int i = 0 ; i < n ;i++)
        {
            l[i]=p[s[i] - 'A'];
            p[s[i] -'A'] =i;
        }
        p =vector<int>(26,n);
        for(int i = n-1 ;i>=0;i--)
        {
            r[i]=p[s[i] - 'A'];
            p[s[i] -'A'] =i;
        }
        int res = 0;
        for(int i = 0 ; i< n ;i++)
        {
            res  +=  (i - l[i]) * (r[i] -i);
        }
        return res;
    }
};