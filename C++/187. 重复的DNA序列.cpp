//https://leetcode.cn/problems/repeated-dna-sequences/description/?envType=daily-question&envId=2023-11-05



class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        int n = s.size();
        vector<string> ans;
        if(n <= 10) return ans;
        unordered_map<string, int> m;
        for(int i = 0; i <= n - 10; i++)
        {
            string tmp = s.substr(i, 10);
            m[tmp]++;
        }
        for(auto str: m)
        {
            if(str.second > 1) ans.push_back(str.first);
        }
        return ans;
    }
};