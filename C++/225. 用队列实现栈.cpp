//https://leetcode.cn/problems/implement-stack-using-queues/description/?envType=daily-question&envId=2024-03-03


class MyStack {
public:
    queue<int> q1;
    queue<int> q2;
    MyStack() {
        
    }
    
    void push(int x) {
        if(this->empty()) q1.push(x);
        else if(q1.empty() || q2.empty())
        {
            if(q1.empty())
            {
                q1.push(x);
                while(!q2.empty())
                {
                    q1.push(q2.front());
                    q2.pop();
                }
            }
            else
            {
                q2.push(x);
                while(!q1.empty())
                {
                    q2.push(q1.front());
                    q1.pop();
                }
            }
        }
    }
    
    int pop() {
        int tmp;
        if(q1.empty()) 
        {
            tmp = q2.front();
            q2.pop();
        }
        else 
        {
            tmp = q1.front();
            q1.pop();
        }
        return tmp;
    }
    
    int top() {
        int tmp;
        if(q1.empty()) 
            tmp = q2.front();
        else 
            tmp = q1.front();
        return tmp;
    }
    
    bool empty() {
        return q1.empty() && q2.empty();
    }
};

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack* obj = new MyStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->top();
 * bool param_4 = obj->empty();
 */