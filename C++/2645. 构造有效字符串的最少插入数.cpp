//https://leetcode.cn/problems/minimum-additions-to-make-valid-string/description/?envType=daily-question&envId=2024-01-11


class Solution {
public:
    int addMinimum(string word) {
       int res = 0;
       char hope = 'a';
    
       for(auto ch : word)
       {
            if(hope != ch) res += (ch - hope + 3) % 3;
            hope = (ch + 1) % 'a' + 'a';
       } 
       if(hope == 'b') res += 2;
        if(hope == 'c') res += 1;
       return res;
    }
};