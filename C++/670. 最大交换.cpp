//https://leetcode.cn/problems/maximum-swap/?envType=daily-question&envId=2024-01-22


class Solution {
public:
    int maximumSwap(int num) {
        string s = to_string(num);
        int n = s.size(); 
        for(int i = 0; i < n; i++)
        {
            char val = s[i];
            int idx;
            for(int j = i + 1; j < n; j++)
            {
                if(s[j] >= val)
                {
                    val = s[j];
                    idx = j;
                }
            }
            if(val != s[i])
            {
                swap(s[i], s[idx]);
                break;
            }
        }
        return atoi(s.data());
    
    
    
    }
};