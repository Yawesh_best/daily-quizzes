//https://leetcode.cn/problems/form-smallest-number-from-two-digit-arrays/?envType=daily-question&envId=2023-09-05

class Solution {
public:
    int minNumber(vector<int>& nums1, vector<int>& nums2) {
        int ans = 10;
        for(int i = 0; i <nums1.size(); i++)
        {
            for(int j = 0; j <nums2.size(); j++)
            {
                if(nums1[i] == nums2[j]) ans = min(ans, nums1[i]);
            }
        }
        if(ans < 10) return ans;
        sort(nums1.begin(), nums1.end());
        sort(nums2.begin(), nums2.end());
        return min(nums1[0], nums2[0]) * 10 + max(nums1[0], nums2[0]);
    }
};