//https://leetcode.cn/problems/find-kth-largest-xor-coordinate-value/?envType=daily-question&envId=2024-05-26https://leetcode.cn/problems/find-kth-largest-xor-coordinate-value/?envType=daily-question&envId=2024-05-26

class Solution {
public:  
    int kthLargestValue(vector<vector<int>>& matrix, int k) {
        int m = matrix.size(), n = matrix[0].size();
        priority_queue<int,vector<int>,greater<int>> q;

        // 算出每一行的异或前缀和
        for(int i = 0; i < m; i++)
            for(int j = 0; j < n; j++)
                if(j != 0) matrix[i][j] ^= matrix[i][j-1];

        // 再算总前缀和  就是该下标的左上部分的异或和，上面计算了左边的，下面再加上上面的异或和就行了
        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                if(i != 0) matrix[i][j] ^= matrix[i-1][j];
                q.push(matrix[i][j]);
                if(q.size() > k) q.pop();
            }
        }

        return q.top();
    }
};