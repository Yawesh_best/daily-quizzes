//https://leetcode.cn/problems/design-hashset/description/?envType=daily-question&envId=2024-04-14https://leetcode.cn/problems/design-hashset/description/?envType=daily-question&envId=2024-04-14
class MyHashSet {
public:
    
    MyHashSet() {
        hash = vector<int> (1000001);
    }
    
    void add(int key) {
        hash[key] = 1;
    }
    
    void remove(int key) {
        if(hash[key]) hash[key]--;
    }
    
    bool contains(int key) {
        return hash[key];
    }
private:
    vector<int> hash;
};
