//https://leetcode.cn/problems/stone-game-vi/description/?envType=daily-question&envId=2024-02-02https://leetcode.cn/problems/stone-game-vi/description/?envType=daily-question&envId=2024-02-02

class Solution {
public:
    int stoneGameVI(vector<int>& aliceValues, vector<int>& bobValues) {
        vector<pair<int, int>> v;
        int n = aliceValues.size();
        for(int i = 0; i < n; i ++) 
            v.push_back({aliceValues[i], bobValues[i]});
        auto cmp = [&](pair<int, int> p1, pair<int, int> p2) -> bool {
            return p1.first + p1.second > p2.first + p2.second;
        };
        sort(v.begin(), v.end(), cmp);

        int sum1 = 0, sum2 = 0;

        for(int i = 0; i < n; i ++)
        {
            if(i % 2 == 0) sum1 += v[i].first;
            else sum2 += v[i].second;
        }

        if(sum1 > sum2) return 1;
        if(sum1 < sum2) return -1;
        return 0;
    }
};class Solution {
public:
    int stoneGameVI(vector<int>& aliceValues, vector<int>& bobValues) {
        vector<pair<int, int>> v;
        int n = aliceValues.size();
        for(int i = 0; i < n; i ++) 
            v.push_back({aliceValues[i], bobValues[i]});
        auto cmp = [&](pair<int, int> p1, pair<int, int> p2) -> bool {
            return p1.first + p1.second > p2.first + p2.second;
        };
        sort(v.begin(), v.end(), cmp);

        int sum1 = 0, sum2 = 0;

        for(int i = 0; i < n; i ++)
        {
            if(i % 2 == 0) sum1 += v[i].first;
            else sum2 += v[i].second;
        }

        if(sum1 > sum2) return 1;
        if(sum1 < sum2) return -1;
        return 0;
    }
};

