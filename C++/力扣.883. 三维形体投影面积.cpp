//https://leetcode.cn/problems/projection-area-of-3d-shapes/


class Solution {
public:
int projectionArea(vector<vector<int>>& grid) {
	int sum = 0;
	for (size_t i = 0; i!= grid.size(); ++i) {
		int max1 = 0,max2=0;
		for (size_t j = 0; j != grid.at(i).size(); ++j) {
			if (grid.at(i).at(j))++sum;
			if (grid.at(i).at(j) > max1)max1 = grid.at(i).at(j);
			if (grid.at(j).at(i) > max2)max2 = grid.at(j).at(i);
		}
		sum += max1+max2;
	}
	return sum;
}
};