//https://leetcode.cn/problems/removing-stars-from-a-string/?envType=daily-question&envId=2024-09-14


class Solution {
public:
    string removeStars(string s) {
        string ans;

        for (auto ch : s)
            if (ch == '*') ans.pop_back();
            else ans.push_back(ch);
        return ans;
    }
};