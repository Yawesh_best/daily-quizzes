//https://leetcode.cn/problems/watering-plants/submissions/530290152/?envType=daily-question&envId=2024-05-08https://leetcode.cn/problems/watering-plants/submissions/530290152/?envType=daily-question&envId=2024-05-08

class Solution {
public:
    int wateringPlants(vector<int>& plants, int capacity) {
        int ans = 0;
        int pos = 1;
        int cur = capacity;
        for (int plant : plants) {
            if (cur >= plant) {
                ans++;
                cur -= plant;
            } else {
                ans += 2 * pos - 1;
                cur = capacity - plant;
            }

            pos++;
        }

        return ans;
    }
};