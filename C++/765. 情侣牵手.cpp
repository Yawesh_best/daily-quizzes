//https://leetcode.cn/problems/couples-holding-hands/description/?envType=daily-question&envId=2023-11-11


class Solution {
public:
    int minSwapsCouples(vector<int>& row) {
        int ans = 0, n = row.size();
        for (int i = 0; i < n; i += 2) 
        {
            int x = row[i];
            int y = x ^ 1;
            if (row[i + 1] != y) 
            {
                for (int j = i + 2; j < n; j++) 
                {
                    if (row[j] == y) 
                    {
                        int temp = row[i + 1];
                        row[i + 1] = row[j];
                        row[j] = temp;
                        ans++;
                        break;
                    }
                }
            }
        }
        return ans;
    }
};