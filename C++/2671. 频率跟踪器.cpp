//https://leetcode.cn/problems/frequency-tracker/?envType=daily-question&envId=2024-03-21


#include <unordered_map>

class FrequencyTracker {
public:
    FrequencyTracker() {

    }
    
    void add(int number) {
        if(!ma[number]) 
        {
            ma[number]++;
            cnt[1]++;    
        }
        else
        {
            cnt[ma[number]]--;
            cnt[ma[number] + 1]++;
            ma[number]++;
        }
    }
    
    void deleteOne(int number) {
        if(ma[number])
        {
            cnt[ma[number]]--;
            cnt[ma[number] - 1]++;
            ma[number]--;
        }
        else return;
    }
    
    bool hasFrequency(int frequency) {
         
        return cnt[frequency];
    }
private:
    std::unordered_map<int, int> cnt;
    std::unordered_map<int, int> ma;
};
