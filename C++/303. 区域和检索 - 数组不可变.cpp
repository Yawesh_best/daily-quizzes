//https://leetcode.cn/problems/range-sum-query-immutable/?envType=daily-question&envId=2024-03-18

class NumArray {
public:
    
    NumArray(vector<int>& nums) {
        int n = nums.size();
        sum.resize(n + 1, 0);
        sum[1] = nums[0];
        for(int i = 1; i < n; i++)
            sum[i + 1] = sum[i] + nums[i];
    }
    
    int sumRange(int left, int right) {
        return sum[right + 1] - sum[left];
    }
private:
    vector<int> sum;
};

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray* obj = new NumArray(nums);
 * int param_1 = obj->sumRange(left,right);
 */