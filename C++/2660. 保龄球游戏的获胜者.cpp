//https://leetcode.cn/problems/determine-the-winner-of-a-bowling-game/?envType=daily-question&envId=2023-12-27https://leetcode.cn/problems/determine-the-winner-of-a-bowling-game/?envType=daily-question&envId=2023-12-27


class Solution {
    public int isWinner(int[] player1, int[] player2) {
        // 模拟
        int sum1 = sum(player1);
        int sum2 = sum(player2);
        return sum1 == sum2 ? 0 : sum1 > sum2 ? 1 : 2;
    }

    public int sum(int[] player) {
        int res = 0;
        for (int i = 0; i < player.length; i++) {
            // 判断前两轮是否有10
            if ((i > 0 && player[i - 1] == 10) || (i > 1 && player[i - 2] >= 10))
                res += 2 * player[i];
            else
                res += player[i];
        }
        return res;
    }
}