//https://leetcode.cn/problems/minimum-equal-sum-of-two-arrays-after-replacing-zeros/


class Solution {
public:
    long long minSum(vector<int>& nums1, vector<int>& nums2) {
        long long sm1 = 0, sm2 = 0;
        // zero1：nums1 中几个 0，zero2：nums2 中几个 0
        int zero1 = 0, zero2 = 0;

        for (int x : nums1) 
        {
            sm1 += max(x, 1);
            if (x == 0) zero1++;
        }
        for (int x : nums2) 
        {
            sm2 += max(x, 1);
            if (x == 0) zero2++;
        }

        // 分类讨论：两个数组都没有 0
        if (zero1 == 0 && zero2 == 0) 
        {
            if (sm1 == sm2) return sm1;
            else return -1;
        // 分类讨论：其中一个有 0
        } 
        else if (zero1 == 0) 
        {
            if (sm1 >= sm2) 
                return sm1;
            else 
                return -1;
        } 
        else if (zero2 == 0) 
        {
            if (sm1 <= sm2) 
                return sm2;
            else 
                return -1;
        // 分类讨论：两者都有 0
        } 
        else 
        {
            return max(sm1, sm2);
        }
    }
};
