//https://leetcode.cn/problems/employee-importance/description/?envType=daily-question&envId=2024-08-26


/*
// Definition for Employee.
class Employee {
public:
    int id;
    int importance;
    vector<int> subordinates;
};
*/

class Solution {
public:
    int getImportance(vector<Employee*> employees, int id) {
        int ans = 0;
        for(auto emp : employees)
        {
            if(emp->id == id)
            {
                ans += emp->importance;
                for(int i = 0; i < emp->subordinates.size(); i++)
                {
                    ans += getImportance(employees, emp->subordinates[i]);
                }
                
                break;
            }
        }
        return ans;
    }
};