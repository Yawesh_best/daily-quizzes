//https://leetcode.cn/problems/split-strings-by-separator/description/?envType=daily-question&envId=Invalid%20Date


class Solution {
public:
    vector<string> splitWordsBySeparator(vector<string>& words, char separator) {
        vector<string> ans;
        int n = words.size();
        for(int i = 0; i < n; i++)
        {
            int left = 0;
            for(int j = 0; j < words[i].size(); ++j)
            {
                if(words[i][j] == separator)
                {
                    string tmp(words[i].begin() + left, words[i].begin() + j);
                    left = j + 1;
                    if(!tmp.empty())
                        ans.push_back(tmp);
                }
            }
            if(left != words[i].size())
            {
                string tmp(words[i].begin() + left, words[i].end());
                if(!tmp.empty()) ans.push_back(tmp);
            }
        }
        return ans;
    }
};