//https://leetcode.cn/problems/count-the-number-of-vowel-strings-in-range/description/?envType=daily-question&envId=2023-11-07



class Solution {
public:
    int vowelStrings(vector<string>& words, int left, int right) {
        unordered_set<char> vowel = {'a', 'e', 'i', 'o', 'u'};
        int ans = 0;
        for(int i = left; i <= right; i++) {
            if (vowel.count(words[i][0]) && vowel.count(words[i][words[i].size() - 1])) {
                ans++;
            }
        }
        return ans;
    }
};