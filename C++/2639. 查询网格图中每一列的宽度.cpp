//https://leetcode.cn/problems/find-the-width-of-columns-of-a-grid/submissions/527509783/?envType=daily-question&envId=2024-04-27


class Solution {
public:
    int count(int n)
    {
        int c = n <= 0;
        while(n){
            c++;
            n /= 10;
        }
        return c;
    }
    vector<int> findColumnWidth(vector<vector<int>>& grid) {
        vector<int> ans;
        int m = grid.size(), n = grid[0].size();
        for(int i = 0; i < n; i++){
            int tmp = 0;
            for(int j = 0; j < m; j++) tmp = max(tmp, count(grid[j][i]));
            ans.push_back(tmp);
        }
        return ans;

    }
};