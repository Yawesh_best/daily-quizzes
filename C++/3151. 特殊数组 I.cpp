//https://leetcode.cn/problems/special-array-i/description/?envType=daily-question&envId=2024-08-13

class Solution {
public:
    bool isArraySpecial(vector<int>& nums) {
        int n = nums.size();
        if(n == 1) return true;
        nums[0] = nums[0] % 2;
        for(int i = 1; i < nums.size(); i++)
        {
            nums[i] = nums[i] % 2;
            if(nums[i - 1] == nums[i]) return false;
        }
        return true;
    }
};