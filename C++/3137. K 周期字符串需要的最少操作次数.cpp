//https://leetcode.cn/problems/minimum-number-of-operations-to-make-word-k-periodic/description/?envType=daily-question&envId=2024-08-17


class Solution {
public:
    int minimumOperationsToMakeKPeriodic(string word, int k) {
        unordered_map<string, int> ma;
        int count = 0, sum = word.size() / k;
        for(int i = 0; i < word.size(); i+=k)
        {
            const string&& tmp = word.substr(i, k);
            ma[tmp]++;
            count = max(count, ma[tmp]);
        }
        return sum - count;
    }
};

