//https://leetcode.cn/problems/find-the-minimum-possible-sum-of-a-beautiful-array/description/?envType=daily-question&envId=2024-03-08https://leetcode.cn/problems/find-the-minimum-possible-sum-of-a-beautiful-array/description/?envType=daily-question&envId=2024-03-08



using ll = long long;
class Solution {
public:
    int minimumPossibleSum(int n, int target) {
        ll x = min(target / 2, n), mod = 1e9 + 7;
        ll s1 = (1 + x) * x / 2;
        ll s2 = (target + target + (n - x - 1)) * max(0LL, n - x) / 2;
        return (s1 + s2) % mod; 
    }
};