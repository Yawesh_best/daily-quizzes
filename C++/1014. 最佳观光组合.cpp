//https://leetcode.cn/problems/best-sightseeing-pair/description/?envType=daily-question&envId=2024-09-23https://leetcode.cn/problems/best-sightseeing-pair/description/?envType=daily-question&envId=2024-09-23

class Solution {
public:
    int maxScoreSightseeingPair(vector<int>& values) {
        int n =values.size(), ans=0, dp1 = 0, dp2 = 0;
        for(int i=1;i<n;i++)
        {
            dp1=max(dp1,values[i-1]+i-1);
            dp2=max(dp2,dp1+values[i]-i);
        }
        return dp2;
    }
};