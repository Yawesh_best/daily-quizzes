//https://leetcode.cn/problems/find-missing-and-repeated-values/


class Solution {
public:
    vector<int> findMissingAndRepeatedValues(vector<vector<int>>& grid) {
        int n = grid.size();
        vector<int> ans(2, 0), nums(n * n + 1, 0);
        for(int i = 0; i < n; i++)
        {
            for(int j = 0; j < n; j++)
            {
                int num = grid[i][j];
                if(nums[num])
                    ans[0] = num;
                else
                    nums[num] = 1;
            }
        }
        for(int i = 1; i <= n * n; i++)
        {
            if(nums[i] == 0)
            {
                ans[1] = i;
                break;
            }
        }
    
    return ans;
    }
};