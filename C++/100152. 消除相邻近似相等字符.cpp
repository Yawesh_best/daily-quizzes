//https://leetcode.cn/problems/remove-adjacent-almost-equal-characters/description/


class Solution {
public:
    int removeAlmostEqualCharacters(string word) {
        int n = word.size();
        vector<int> dp(n, 0);
        for(int i = 1; i < n; i++)
        {
            dp[i] = dp[i - 1];
            if(word[i - 1] != '1' && abs(word[i - 1] - word[i]) <= 1)
            {
                dp[i] = dp[i - 1] + 1;
                word[i] = '1';
            }
            
        }
        return dp[n - 1];
    }
};