//https://leetcode.cn/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/?envType=daily-question&envId=2024-04-03

class Solution {
public:
    TreeNode* getTargetCopy(TreeNode* original, TreeNode* cloned, TreeNode* target) {
        function<TreeNode*(TreeNode*)> dfs = [&](TreeNode* node) -> TreeNode* {
            if (!node) {
                return nullptr;
            }
            if (node->val == target->val) {
                return node;
            }
            TreeNode* res = dfs(node->left);
            return res ? res : dfs(node->right);
        };

        return dfs(cloned);
    }
};