//https://leetcode.cn/problems/maximize-number-of-subsequences-in-a-string/description/?envType=daily-question&envId=2024-09-24https://leetcode.cn/problems/maximize-number-of-subsequences-in-a-string/description/?envType=daily-question&envId=2024-09-24

class Solution {
public:
    long long maximumSubsequenceCount(string text, string pattern) {
        long long target1 = 0,target2 = 0,nums=0;
        for(long long i =0;i<text.size();i++){
            if(text[i] == pattern[0])nums++;
            else if(text[i] == pattern[1] && pattern[0] != pattern[1]){
                target1 += (nums+1);
                target2 += nums;
            }
        }
        if(pattern[0] == pattern[1]) return (nums+1)*nums/2;
        target2 += nums;
        return target1>target2?target1:target2;
    }
};