//https://leetcode.cn/problems/find-the-array-concatenation-value/?envType=daily-question&envId=2023-10-12


class Solution {
public:
    long long findTheArrayConcVal(vector<int>& nums) {
        int n = nums.size(), left = 0, right = n - 1;
        long long ans = 0;
        while(left < right)
        {
            int l = nums[left], r = nums[right];
            while(r)
            {
                l *= 10;
                r /= 10;
            }
            ans += l + nums[right];
            left++;
            right--;
        }
        if(left == right) ans += nums[left];
        return ans;
    }
};