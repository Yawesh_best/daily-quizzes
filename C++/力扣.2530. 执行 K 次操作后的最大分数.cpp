//https://leetcode.cn/problems/maximal-score-after-applying-k-operations/?envType=daily-question&envId=2023-10-18


class Solution {
public:
    long long maxKelements(std::vector<int>& nums, int k) {
        priority_queue<int, vector<int>> pq;

        for(int num : nums) 
        {
            pq.push(num);
        }

        long long ans = 0;
        while(k--) 
        {
            int num = pq.top();
            pq.pop();
            ans += num;
            pq.push(ceil(num / 3.0));
        }

        return ans;
    }
};