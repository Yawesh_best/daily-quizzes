//https://leetcode.cn/problems/lexicographically-smallest-string-after-a-swap/description/?envType=daily-question&envId=2024-10-30

class Solution {
public:
    string getSmallestString(string s) {
        int n = s.size();
        for(int i = 0; i < n - 1; i++)
        {
            if((s[i] - '0') % 2 == (s[i + 1] - '0') % 2)
            {
                if((s[i] - '0') > (s[i + 1] - '0'))
                {
                    char tmp = s[i];
                    s[i] = s[i + 1];
                    s[i + 1] = tmp;
                    break;
                }
            }
        }
        return s;

    }
};