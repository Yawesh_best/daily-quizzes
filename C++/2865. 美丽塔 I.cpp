//https://leetcode.cn/problems/beautiful-towers-i/?envType=daily-question&envId=2024-01-24



class Solution {
public:
    long long maximumSumOfHeights(std::vector<int>& a) {
        long long ans = 0;
        int n = a.size();

        for (int i = 0; i < n; i++) {
            long long s = a[i], mn = a[i];

            for (int j = i - 1; j >= 0; j--) {
                mn = std::min(mn, (long long)a[j]);
                s += mn;
            }

            mn = a[i];
            for (int j = i + 1; j < n; j++) {
                mn = std::min(mn, (long long)a[j]);
                s += mn;
            }

            ans = std::max(ans, s);
        }

        return ans;
    }
};