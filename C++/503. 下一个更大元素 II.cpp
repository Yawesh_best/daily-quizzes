//https://leetcode.cn/problems/next-greater-element-ii/description/?envType=daily-question&envId=2024-06-24


class Solution {
public:
    vector<int> nextGreaterElements(vector<int>& nums) {
        stack<int> s; 
        vector<int> ret(nums.size(), -1); 
        int n = nums.size();

        for (int i = 0; i < 2 * n; i++) {
            int index = i % n; 

            while (!s.empty() && nums[index] > nums[s.top()]) {
                ret[s.top()] = nums[index];
                s.pop();
            }

            if (i < n) {
                s.push(index);
            }
        }

        return ret;
    }
};