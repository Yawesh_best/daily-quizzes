//https://leetcode.cn/problems/pseudo-palindromic-paths-in-a-binary-tree/?envType=daily-question&envId=2023-11-25


/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int ans = 0;
    int count[10];
    void pseudo(TreeNode* root)
    {
        if(root == nullptr) return;
        count[root->val]++;
        if(root->left == nullptr && root->right == nullptr)
        {
            int count_one = 0;
            for(int i = 1; i <= 9; ++i)
            {
                count_one += count[i] % 2; 
            }
            if(count_one == 1 || count_one == 0)
                ans++;    
        }
        pseudo(root->left);
        pseudo(root->right);
        count[root->val]--;
    }
    int pseudoPalindromicPaths (TreeNode* root) {
        
        pseudo(root);
        return ans;
    }
};