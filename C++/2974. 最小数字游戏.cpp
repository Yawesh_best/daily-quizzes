//https://leetcode.cn/problems/minimum-number-game/?envType=daily-question&envId=2024-07-12


class Solution {
public:
    vector<int> numberGame(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        int n = nums.size();
        vector<int> ans(n);
        for(int i = 1; i < n; i += 2)
        {
            ans[i - 1] = nums[i];
            ans[i] = nums[i - 1];
        }
        return ans;
    }
};