//KY11 ������������https://www.nowcoder.com/practice/4b91205483694f449f94c179883c1fef?tpId=60&&tqId=29483&rp=1&ru=/activity/oj&qru=/ta/tsing-kaoyan/question-ranking

#include <stdio.h>
typedef char BTDataType;
typedef struct BinaryTreeNode
{
	BTDataType data;
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
}BTNode;
BTNode* BuyNode(BTDataType x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	if (node == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	node->data = x;
	node->left = NULL;
	node->right = NULL;

	return node;
}
BTNode* BTcreat(char* arr, int* i)
{
	if (arr[*i] == '#')
	{
		(*i)++;
		return NULL;
	}
	BTNode* root = BuyNode(arr[*i]);
	(*i)++;
	root->left = BTcreat(arr, i);
	root->right = BTcreat(arr, i);
	return root;
}
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	BinaryTreePostOrder(root->left);
	printf("%c ", root->data);
	BinaryTreePostOrder(root->right);

}
int main()
{
	int i = 0;
	char arr[100];
	scanf("%s", arr);
	BTNode* root = BTcreat(arr, &i);
	BinaryTreePostOrder(root);
	return 0;
}